<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'about'])->latest()->get();

        return view('admin.about.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'video_url' => 'nullable|max:255',
            'description' => 'nullable|max:100000',
        ]);
        $rx = '(//www.youtube(?:-nocookie)?.com/(?:v|embed)/([a-zA-Z0-9-_]+).*)';

        if(!empty($request->video_url) && !preg_match($rx, $request->video_url, $matches)){
            return redirect()->back()->withInput()->withErrors(['video_url' => 'This is not youtube embed url.']);
        }
        $request->request->add(['slug' => Str::slug($request->title, '-'), 'type' => 'about', 'status' => 1, 'is_video' => 1, 'user_id' => auth()->user()->id]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'about'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }
        Post::create($request->all());
        Session::flash('flash_message', 'About Us added!');

        return redirect('admin/about');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('type', 'about')->findOrFail($id);
        return view('admin.about.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'about')->findOrFail($id);
        return view('admin.about.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'about')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'video_url' => 'nullable|max:255',
            'description' => 'nullable|max:100000',
        ]);

        $rx = '(//www.youtube(?:-nocookie)?.com/(?:v|embed)/([a-zA-Z0-9-_]+).*)';
        if(!empty($request->video_url) && !preg_match($rx, $request->video_url, $matches)){
            return redirect()->back()->withInput()->withErrors(['video_url' => 'This is not youtube embed url.']);
        }

        $request->request->add(['slug' => Str::slug($request->title, '-')]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'about'])->first();
            if(!empty($post)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }

        $post->update($request->all());

        Session::flash('flash_message', 'About Us updated!');

        return redirect('admin/about');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'about')->findOrFail($id);
        $post->delete();

        Session::flash('flash_message', 'About Us deleted!');

        return redirect('admin/about');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'about')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'About Us published!');

        return redirect('admin/about');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'about')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'About Us unpublished!');

        return redirect('admin/about');
    }

    public function sorting(){
        $posts = Post::where(['type' => 'about', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'about')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.about.sorting', compact('posts', 'options'));
    }
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'about']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'About sorted!');

        return redirect('admin/about');
    }
}
