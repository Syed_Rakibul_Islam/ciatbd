<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::where(['type' => 'contact', 'slug' => 'contact'])->firstOrFail();

        return view('admin.contact.index', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $post = Post::where(['type' => 'contact', 'slug' => 'contact'])->firstOrFail();
        return view('admin.contact.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $post = Post::where(['type' => 'contact', 'slug' => 'contact'])->firstOrFail();
        $this->validate($request, [
            'title' => 'required|max:150',
            'description' => 'nullable|max:10000',
            'address' => 'nullable|max:500',
            'email' => 'nullable|max:150',
            'contact' => 'nullable|max:150'
        ]);

        $request->merge(['options' => json_encode(['address' => $request->address, 'email' => $request->email, 'contact' => $request->contact])]);

        $post->update($request->all());

        Session::flash('flash_message', 'Contact updated!');

        return redirect('admin/contact');
    }
}
