<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Client;
use App\Partner;
use App\Post;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
	    $posts = Post::select('id', 'type')->get();
		return view('admin.dashboard', compact('posts'));
	}
}
