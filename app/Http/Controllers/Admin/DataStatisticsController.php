<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Session;

class DataStatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'data-statistics'])->latest()->get();

        return view('admin.data-statistics.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.data-statistics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'description' => 'nullable|max:100000',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'data-statistics', 'status' => 1, 'user_id' => auth()->user()->id, 'options' => json_encode(['designation' => $request->designation])]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'data-statistics'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        Post::create($request->all());
        Session::flash('flash_message', 'Data & Statistics added!');

        return redirect('admin/data-statistics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where(['type' => 'data-statistics'])->findOrFail($id);

        return view('admin.data-statistics.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'data-statistics')->findOrFail($id);
        return view('admin.data-statistics.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'data-statistics')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'description' => 'nullable|max:100000'
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'options' => json_encode(['designation' => $request->designation])]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'data-statistics'])->first();
            if(!empty($oldPost)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }

        $post->update($request->all());

        Session::flash('flash_message', 'Data & Statistics updated!');

        return redirect('admin/data-statistics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'data-statistics')->findOrFail($id);
        $post->categories()->detach();

        $path = 'images/upload/data-statistics/';
        if(!empty($post->image) && file_exists(($path . $post->image))){
            unlink($path . $post->image);
        }

        $post->delete();

        Session::flash('flash_message', 'Data & Statistics deleted!');

        return redirect('admin/data-statistics');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'data-statistics')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Data & Statistics published!');

        return redirect('admin/data-statistics');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'data-statistics')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Data & Statistics unpublished!');

        return redirect('admin/data-statistics');
    }
    public function sorting(){
        $posts = Post::where(['type' => 'data-statistics', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'data-statistics')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.data-statistics.sorting', compact('posts', 'options'));
    }
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'data-statistics']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'Data & Statistics sorted!');

        return redirect('admin/data-statistics');
    }
}
