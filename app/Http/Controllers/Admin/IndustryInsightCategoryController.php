<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class IndustryInsightCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post_id)
    {
        $post = Post::where('type', 'industry-insight')->with('categories')->findOrFail($post_id);

        return view('admin.industry-insight.category.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($post_id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        return view('admin.industry-insight.category.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($post_id, Request $request)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);

        $this->validate($request, [
            'title' => 'required|max:150',
            'feature_image' => 'required|max:2000',
            'url' => 'required|url|max:250'
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'industry-insight', 'user_id' => auth()->user()->id, 'status' => 1, 'options' => json_encode(['url' => $request->url])]);
        $oldCategory = Category::where(['slug' => $request->slug, 'type' => 'industry-insight'])->first();
        if(!empty($oldCategory)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/industry-insight/category/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        $category = Category::create($request->all());

        $category->posts()->attach([$post->id]);

        Session::flash('flash_message', 'Industry Insight category added!');

        return redirect('admin/industry-insight/' . $post->id . '/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($post_id, $id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        $category = Category::where('type', 'industry-insight')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);
        return view('admin.industry-insight.category.show', compact('post', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($post_id, $id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        $category = Category::where('type', 'industry-insight')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        return view('admin.industry-insight.category.edit', compact('post', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($post_id, Request $request, $id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        $category = Category::where('type', 'industry-insight')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $this->validate($request, [
            'title' => 'required|max:150',
            'feature_image' => 'nullable|max:2000',
            'url' => 'required|url|max:250'
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'options' => json_encode(['url' => $request->url])]);

        if($category->slug != $request->slug){
            $oldCategory = Category::where(['slug' => $request->slug, 'type' => 'industry-insight'])->first();
            if(!empty($oldCategory)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/industry-insight/category/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($category->image) && file_exists(($path . $category->image))){
                unlink($path . $category->image);
            }
        }

        if ($request->file('feature_file')){
            $temp = $request->file('feature_file');
            $path = 'files/upload/industry-insight/category/';
            $file_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$file_name;
            $temp->move($path, $full_name);
            $request->merge(['options' => json_encode(['file' => $full_name])]);

            $file = @json_decode(@$category->options)->file;
            if(!empty($file) && file_exists(($path . $file))){
                unlink($path . $file);
            }
        }

        $category->update($request->all());

        Session::flash('flash_message', 'Industry Insight category updated!');

        return redirect('admin/industry-insight/' . $post->id . '/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_id, $id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        $category = Category::where('type', 'industry-insight')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $category->posts()->detach();
        $category->delete();

        Session::flash('flash_message', 'Industry Insight category deleted!');

        return redirect('admin/industry-insight/' . $post->id . '/category');
    }
    public function publish($post_id, $id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        $category = Category::where('type', 'industry-insight')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $category->update(['status' => 1]);

        Session::flash('flash_message', 'Industry Insight category published!');

        return redirect('admin/industry-insight/' . $post->id . '/category');
    }
    public function unpublish($post_id, $id)
    {
        $post = Post::where('type', 'industry-insight')->findOrFail($post_id);
        $category = Category::where('type', 'industry-insight')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $category->update(['status' => null]);

        Session::flash('flash_message', 'Industry Insight category unpublished!');

        return redirect('admin/industry-insight/' . $post->id . '/category');
    }
}
