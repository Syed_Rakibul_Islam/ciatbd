<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class InsightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'insight'])->latest()->get();

        return view('admin.insight.index', compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('type', 'insight')->findOrFail($id);
        return view('admin.insight.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'insight')->findOrFail($id);
        return view('admin.insight.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'insight')->findOrFail($id);
        $this->validate($request, [
            'feature_image' => 'nullable|max:1000',
            'description' => 'required|max:100000'
        ]);

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/wcu/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($post->image) && file_exists(($path . $post->image))){
                unlink($path . $post->image);
            }
        }

        $post->update($request->all());

        Session::flash('flash_message', 'Insight updated!');

        return redirect('admin/insight');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'insight')->findOrFail($id);
        $post->categories()->detach();

        $post->delete();

        Session::flash('flash_message', 'Insight deleted!');

        return redirect('admin/insight');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'insight')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Insight published!');

        return redirect('admin/insight');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'insight')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Insight unpublished!');

        return redirect('admin/insight');
    }
}
