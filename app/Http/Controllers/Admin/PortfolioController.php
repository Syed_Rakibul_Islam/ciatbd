<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Session;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'portfolio'])->latest()->get();

        return view('admin.portfolio.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.portfolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'feature_image' => 'required|max:2000',
            'description' => 'nullable|max:100000',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'portfolio', 'status' => 1, 'user_id' => auth()->user()->id, 'options' => json_encode(['designation' => $request->designation])]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'portfolio'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/portfolio/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        Post::create($request->all());
        Session::flash('flash_message', 'Portfolio added!');

        return redirect('admin/portfolio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where(['type' => 'portfolio'])->findOrFail($id);

        return view('admin.portfolio.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'portfolio')->findOrFail($id);
        return view('admin.portfolio.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'portfolio')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'feature_image' => 'nullable|max:2000',
            'description' => 'nullable|max:100000'
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'options' => json_encode(['designation' => $request->designation])]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'portfolio'])->first();
            if(!empty($oldPost)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }
        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/portfolio/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($post->image) && file_exists(($path . $post->image))){
                unlink($path . $post->image);
            }
        }
        $post->update($request->all());

        Session::flash('flash_message', 'Portfolio updated!');

        return redirect('admin/portfolio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'portfolio')->findOrFail($id);
        $post->categories()->detach();

        $path = 'images/upload/portfolio/';
        if(!empty($post->image) && file_exists(($path . $post->image))){
            unlink($path . $post->image);
        }

        $post->delete();

        Session::flash('flash_message', 'Portfolio deleted!');

        return redirect('admin/portfolio');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'portfolio')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Portfolio published!');

        return redirect('admin/portfolio');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'portfolio')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Portfolio unpublished!');

        return redirect('admin/portfolio');
    }
    public function sorting(){
        $posts = Post::where(['type' => 'portfolio', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'portfolio')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.portfolio.sorting', compact('posts', 'options'));
    }
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'portfolio']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'Portfolio sorted!');

        return redirect('admin/portfolio');
    }
}
