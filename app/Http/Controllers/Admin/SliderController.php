<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'slider'])->latest()->get();

        return view('admin.slider.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'description' => 'nullable|max:250',
            'feature_image' => 'nullable|max:2000',
            'button_1_name' => 'required|max:150',
            'button_1_url' => 'required|url|max:500',
            'button_2_name' => 'required|max:150',
            'button_2_url' => 'required|url|max:500',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'slider', 'status' => 1, 'user_id' => auth()->user()->id, 'options' => json_encode(['button_1_name' => $request->button_1_name, 'button_1_url' => $request->button_1_url, 'button_2_name' => $request->button_2_name, 'button_2_url' => $request->button_2_url])]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'slider'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/slider/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        Post::create($request->all());
        Session::flash('flash_message', 'Slider added!');

        return redirect('admin/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('type', 'slider')->findOrFail($id);
        return view('admin.slider.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'slider')->findOrFail($id);
        return view('admin.slider.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'slider')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'description' => 'nullable|max:250',
            'feature_image' => 'nullable|max:2000',
            'button_1_name' => 'required|max:150',
            'button_1_url' => 'required|url|max:500',
            'button_2_name' => 'required|max:150',
            'button_2_url' => 'required|url|max:500',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-')]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'slider', 'options' => json_encode(['button_1_name' => $request->button_1_name, 'button_1_url' => $request->button_1_url, 'button_2_name' => $request->button_2_name, 'button_2_url' => $request->button_2_url])])->first();
            if(!empty($oldPost)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }
        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/slider/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($post->image) && file_exists(($path . $post->image))){
                unlink($path . $post->image);
            }
        }
        $post->update($request->all());

        Session::flash('flash_message', 'Slider updated!');

        return redirect('admin/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'slider')->findOrFail($id);

        $path = 'images/upload/slider/';
        if(!empty($post->image) && file_exists(($path . $post->image))){
            unlink($path . $post->image);
        }

        $post->delete();

        Session::flash('flash_message', 'Slider deleted!');

        return redirect('admin/slider');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'slider')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Slider published!');

        return redirect('admin/slider');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'slider')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Slider unpublished!');

        return redirect('admin/slider');
    }

    public function sorting(){
        $posts = Post::where(['type' => 'slider', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'slider')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.slider.sorting', compact('posts', 'options'));
    }
    
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'slider']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'Slider sorted!');

        return redirect('admin/slider');
    }
}
