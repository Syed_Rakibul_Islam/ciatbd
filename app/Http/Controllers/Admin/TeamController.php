<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Session;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'team'])->latest()->get();

        return view('admin.team.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'designation' => 'required|max:150',
            'feature_image' => 'required|max:500',
            'description' => 'nullable|max:100000',
            'linkedin' => 'nullable|url|max:250'
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'team', 'status' => 1, 'user_id' => auth()->user()->id, 'options' => json_encode(['designation' => $request->designation, 'linkedin' => $request->linkedin])]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'team'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/team/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        Post::create($request->all());
        Session::flash('flash_message', 'Team added!');

        return redirect('admin/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where(['type' => 'team'])->findOrFail($id);

        return view('admin.team.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'team')->findOrFail($id);
        return view('admin.team.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'team')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'designation' => 'required|max:150',
            'feature_image' => 'nullable|max:500',
            'description' => 'nullable|max:100000',
            'linkedin' => 'nullable|url|max:250'
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'options' => json_encode(['designation' => $request->designation, 'linkedin' => $request->linkedin])]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'team'])->first();
            if(!empty($oldPost)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }
        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/team/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($post->image) && file_exists(($path . $post->image))){
                unlink($path . $post->image);
            }
        }
        $post->update($request->all());

        Session::flash('flash_message', 'Team updated!');

        return redirect('admin/team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'team')->findOrFail($id);

        $path = 'images/upload/team/';
        if(!empty($post->image) && file_exists(($path . $post->image))){
            unlink($path . $post->image);
        }

        $post->delete();

        Session::flash('flash_message', 'Team deleted!');

        return redirect('admin/team');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'team')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Team published!');

        return redirect('admin/team');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'team')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Team unpublished!');

        return redirect('admin/team');
    }
    public function sorting(){
        $posts = Post::where(['type' => 'team', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'team')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.team.sorting', compact('posts', 'options'));
    }
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'team']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'Team sorted!');

        return redirect('admin/team');
    }
    
}
