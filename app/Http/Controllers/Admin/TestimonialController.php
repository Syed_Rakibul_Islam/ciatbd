<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Session;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'testimonial'])->latest()->get();

        return view('admin.testimonial.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'designation' => 'required|max:150',
            'feature_image' => 'required|max:1000',
            'description' => 'nullable|max:100000',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'testimonial', 'status' => 1, 'user_id' => auth()->user()->id, 'options' => json_encode(['designation' => $request->designation])]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'testimonial'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/testimonial/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        Post::create($request->all());
        Session::flash('flash_message', 'Testimonial added!');

        return redirect('admin/testimonial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where(['type' => 'testimonial'])->findOrFail($id);

        return view('admin.testimonial.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'testimonial')->findOrFail($id);
        return view('admin.testimonial.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'testimonial')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'designation' => 'required|max:150',
            'feature_image' => 'nullable|max:1000',
            'description' => 'nullable|max:100000',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'options' => json_encode(['designation' => $request->designation])]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'testimonial'])->first();
            if(!empty($oldPost)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }
        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/testimonial/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($post->image) && file_exists(($path . $post->image))){
                unlink($path . $post->image);
            }
        }
        $post->update($request->all());

        Session::flash('flash_message', 'Testimonial updated!');

        return redirect('admin/testimonial');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'testimonial')->findOrFail($id);

        $path = 'images/upload/testimonial/';
        if(!empty($post->image) && file_exists(($path . $post->image))){
            unlink($path . $post->image);
        }

        $post->delete();

        Session::flash('flash_message', 'Testimonial deleted!');

        return redirect('admin/testimonial');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'testimonial')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Testimonial published!');

        return redirect('admin/testimonial');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'testimonial')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Testimonial unpublished!');

        return redirect('admin/testimonial');
    }
    public function sorting(){
        $posts = Post::where(['type' => 'testimonial', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'testimonial')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.testimonial.sorting', compact('posts', 'options'));
    }
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'testimonial']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'Testimonial sorted!');

        return redirect('admin/testimonial');
    }
}
