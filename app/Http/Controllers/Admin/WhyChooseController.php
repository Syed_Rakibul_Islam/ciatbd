<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class WhyChooseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where(['type' => 'wcu'])->latest()->get();

        return view('admin.wcu.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.wcu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:150',
            'feature_image' => 'nullable|max:500',
            'description' => 'nullable|max:100000',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'wcu', 'status' => 1, 'user_id' => auth()->user()->id]);
        $post = Post::where(['slug' => $request->slug, 'type' => 'wcu'])->first();
        if(!empty($post)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/wcu/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        Post::create($request->all());
        Session::flash('flash_message', 'Why Choose Us added!');

        return redirect('admin/why-choose');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('type', 'wcu')->findOrFail($id);
        return view('admin.wcu.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('type', 'wcu')->findOrFail($id);
        return view('admin.wcu.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('type', 'wcu')->findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:150',
            'feature_image' => 'nullable|max:500',
            'description' => 'nullable|max:100000',
        ]);

        $request->merge(['slug' => Str::slug($request->title, '-')]);

        if($post->slug != $request->slug){
            $oldPost = Post::where(['slug' => $request->slug, 'type' => 'wcu'])->first();
            if(!empty($oldPost)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }
        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/wcu/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($post->image) && file_exists(($path . $post->image))){
                unlink($path . $post->image);
            }
        }
        $post->update($request->all());

        Session::flash('flash_message', 'Why Choose Us updated!');

        return redirect('admin/why-choose');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('type', 'wcu')->findOrFail($id);

        $path = 'images/upload/wcu/';
        if(!empty($post->image) && file_exists(($path . $post->image))){
            unlink($path . $post->image);
        }

        $post->delete();

        Session::flash('flash_message', 'Why Choose Us deleted!');

        return redirect('admin/why-choose');
    }
    public function publish($id)
    {
        $post = Post::where('type', 'wcu')->findOrFail($id);
        $post->update(['status' => 1]);

        Session::flash('flash_message', 'Why Choose Us published!');

        return redirect('admin/why-choose');
    }
    public function unpublish($id)
    {
        $post = Post::where('type', 'wcu')->findOrFail($id);
        $post->update(['status' => null]);

        Session::flash('flash_message', 'Why Choose Us unpublished!');

        return redirect('admin/why-choose');
    }
    public function sorting(){
        $posts = Post::where(['type' => 'wcu', 'status' => 1])->latest()->select('id', 'title')->get();
        $setting = Setting::where('name', 'wcu')->select('options')->first();
        $options = @json_decode(@$setting->options);
        return view('admin.wcu.sorting', compact('posts', 'options'));
    }
    public function saveSorting(Request $request){
        $this->validate($request, [
            'options' => 'required|max:150',
        ]);
        $setting = Setting::firstOrNew(['name' => 'wcu']);
        $setting->options = json_encode($request->options);
        $setting->save();

        Session::flash('flash_message', 'Why Choose Us sorted!');

        return redirect('admin/why-choose');
    }
}
