<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Session;

class WorkshopAndInterviewCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post_id)
    {
        $post = Post::where('type', 'workshop-and-interview')->with('categories')->findOrFail($post_id);

        return view('admin.workshop-and-interview.category.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($post_id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        return view('admin.workshop-and-interview.category.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($post_id, Request $request)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);

        $this->validate($request, [
            'title' => 'required|max:150',
            'video_url' => 'required|url|max:500',
        ]);
        $rx = '(//www.youtube(?:-nocookie)?.com/(?:v|embed)/([a-zA-Z0-9-_]+).*)';

        if(!empty($request->video_url) && !preg_match($rx, $request->video_url, $matches)){
            return redirect()->back()->withInput()->withErrors(['video_url' => 'This is not youtube embed url.']);
        }

        $request->merge(['slug' => Str::slug($request->title, '-'), 'type' => 'workshop-and-interview', 'user_id' => auth()->user()->id, 'status' => 1]);
        $oldCategory = Category::where(['slug' => $request->slug, 'type' => 'workshop-and-interview'])->first();
        if(!empty($oldCategory)){
            return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
        }

        $category = Category::create($request->all());

        $category->posts()->attach([$post->id]);

        Session::flash('flash_message', 'Workshop and Interview category added!');

        return redirect('admin/workshop-and-interview/' . $post->id . '/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($post_id, $id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        $category = Category::where('type', 'workshop-and-interview')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);
        return view('admin.workshop-and-interview.category.show', compact('post', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($post_id, $id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        $category = Category::where('type', 'workshop-and-interview')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        return view('admin.workshop-and-interview.category.edit', compact('post', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($post_id, Request $request, $id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        $category = Category::where('type', 'workshop-and-interview')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $this->validate($request, [
            'title' => 'required|max:150',
            'video_url' => 'required|url|max:500',
        ]);
        $rx = '(//www.youtube(?:-nocookie)?.com/(?:v|embed)/([a-zA-Z0-9-_]+).*)';

        if(!empty($request->video_url) && !preg_match($rx, $request->video_url, $matches)){
            return redirect()->back()->withInput()->withErrors(['video_url' => 'This is not youtube embed url.']);
        }

        $request->merge(['slug' => Str::slug($request->title, '-')]);

        if($category->slug != $request->slug){
            $oldCategory = Category::where(['slug' => $request->slug, 'type' => 'workshop-and-interview'])->first();
            if(!empty($oldCategory)){
                return redirect()->back()->withInput()->withErrors(['title' => 'Title matched. Please add different title.']);
            }
        }

        $category->update($request->all());

        Session::flash('flash_message', 'Workshop and Interview category updated!');

        return redirect('admin/workshop-and-interview/' . $post->id . '/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_id, $id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        $category = Category::where('type', 'workshop-and-interview')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $category->posts()->detach();
        $category->delete();

        Session::flash('flash_message', 'Workshop and Interview category deleted!');

        return redirect('admin/workshop-and-interview/' . $post->id . '/category');
    }
    public function publish($post_id, $id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        $category = Category::where('type', 'workshop-and-interview')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $category->update(['status' => 1]);

        Session::flash('flash_message', 'Workshop and Interview category published!');

        return redirect('admin/workshop-and-interview/' . $post->id . '/category');
    }
    public function unpublish($post_id, $id)
    {
        $post = Post::where('type', 'workshop-and-interview')->findOrFail($post_id);
        $category = Category::where('type', 'workshop-and-interview')->whereHas('posts', function($query) use($post){$query->where('post_id', $post->id);})->findOrFail($id);

        $category->update(['status' => null]);

        Session::flash('flash_message', 'Workshop and Interview category unpublished!');

        return redirect('admin/workshop-and-interview/' . $post->id . '/category');
    }
}
