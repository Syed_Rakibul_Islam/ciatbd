<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\CareerMail;
use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $options = Setting::where('name', 'slider')->orWhere('name', 'wcu')->orWhere('name', 'client')->orWhere('name', 'testimonial')->orWhere('name', 'solution')->orWhere('name', 'portfolio')->orWhere('name', 'blog')->select('options', 'name')->get();
        $postIds = [];
        foreach($options as $option){
            $temArray = array_merge(json_decode($option->options), $postIds);
            $postIds = $temArray;
        }
        $items = Post::where('status', 1)->whereIn('id', $postIds)->get();
        return view('home', compact('items', 'options'));
    }

    public function about(){
        $items = Post::where('status', 1)->where('type', 'about')->orWhere('type', 'wcu')->orWhere('type', 'team')->get();
        return view('about', compact('items'));
    }
    public function solutions(){
        $items = Post::where(['status' => 1, 'type' => 'solution'])->get();
        return view('solutions', compact('items'));
    }
    public function solution($slug){
        $item = Post::where(['status' => 1, 'type' => 'solution', 'slug' => $slug])->with('categories')->firstOrFail();
        $solution_type = @json_decode(@$item->options)->solution_type;
        if($solution_type == 'category_video'){
            return view('solution-video', compact('item'));
        }
        elseif ($solution_type == 'simple'){
            return view('solution', compact('item'));
        }
        return view('solution-category', compact('item', 'solution_type'));
    }
    public function insights(){
        $items = Post::where(['status' => 1, 'type' => 'insight'])->get();

        return view('insights', compact('items'));
    }
    public function dataStatistics(){
        $items = Post::where(['status' => 1, 'type' => 'data-statistics'])->get();

        return view('data-statistics', compact('items'));
    }
    public function singleDataStatistics($slug){
        $item = Post::where(['status' => 1, 'type' => 'data-statistics', 'slug' => $slug])->firstOrFail();
        $categories = Category::where(['status' => 1, 'type' => 'data-statistics'])->whereHas('posts', function($query) use($item){$query->where('post_id', $item->id);})->get();

        return view('single-data-statistics', compact('item', 'categories'));
    }
    public function industryInsight(){
        $items = Post::where(['status' => 1, 'type' => 'industry-insight'])->get();

        return view('industry-insight', compact('items'));
    }
    public function singleIndustryInsight($slug){
        $item = Post::where(['status' => 1, 'type' => 'industry-insight', 'slug' => $slug])->firstOrFail();
        $categories = Category::where(['status' => 1, 'type' => 'industry-insight'])->whereHas('posts', function($query) use($item){$query->where('post_id', $item->id);})->get();

        return view('single-industry-insight', compact('item', 'categories'));
    }
    public function portfolio(){
        $items = Post::where(['status' => 1, 'type' => 'portfolio'])->get();

        return view('portfolio', compact('items'));
    }
    public function singlePortfolio($slug){
        $item = Post::where(['status' => 1, 'type' => 'portfolio', 'slug' => $slug])->firstOrFail();
        $categories = Category::where(['status' => 1, 'type' => 'portfolio'])->whereHas('posts', function($query) use($item){$query->where('post_id', $item->id);})->get();

        return view('single-portfolio', compact('item', 'categories'));
    }
    public function blog(){
        $items = Post::where(['status' => 1, 'type' => 'blog'])->get();

        return view('blog', compact('items'));
    }
    public function singleBlog($slug){
        $item = Post::where(['status' => 1, 'type' => 'blog', 'slug' => $slug])->firstOrFail();

        return view('single-blog', compact('item'));
    }
    public function workshopAndInterview(){
        $items = Post::where(['status' => 1, 'type' => 'workshop-and-interview'])->get();

        return view('workshop-and-interview', compact('items'));
    }
    public function singleWorkshopAndInterview($slug){
        $item = Post::where(['status' => 1, 'type' => 'workshop-and-interview', 'slug' => $slug])->firstOrFail();
        $categories = Category::where(['status' => 1, 'type' => 'workshop-and-interview'])->whereHas('posts', function($query) use($item){$query->where('post_id', $item->id);})->get();

        return view('single-workshop-and-interview', compact('item', 'categories'));
    }
    public function clients(){
        $items = Post::where(['status' => 1, 'type' => 'client'])->get();
        return view('clients', compact('items'));
    }
    public function sendCareer(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'work_type' => 'required',
            'work_field' => 'required',
            'work_prefer' => 'required',
            'msg' => 'required',
            'file' => 'required'
        ]);
        $full_name = '';
        if ($request->file('file')){
            $temp = $request->file('file');
            $path = 'files/upload/career/';
            $file_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$file_name;
            $temp->move($path, $full_name);
        }

        try{
            Mail::to(env('MAIL_SEND_ADDRESS'))->send(new CareerMail($request->name, $request->work_type, $request->work_field, $request->work_prefer, $request->msg, $full_name));
        }
        catch(Exception $e){
            logger($e);
            return redirect('career')->withErrors(['msg' => 'Mail sending error. Please try again']);
        }

        Session::flash('flash_message', 'Sent your information! Thank you for sharing your information with us');

        return redirect('career');
    }
    public function contact(){
        $item = Post::where(['type' => 'contact', 'slug' => 'contact'])->firstOrFail();
        return view('contact', compact('item'));
    }
}
