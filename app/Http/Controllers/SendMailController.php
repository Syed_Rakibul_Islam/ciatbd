<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use App\Mail\ConversationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class SendMailController extends Controller
{
    public function sendContact(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'msg' => 'required'
        ]);

        if ( $validator->fails() ) {
            $fields = "";
            if( !isset( $request->name ) || empty( $request->name ) ) {
                $fields .= "Name";
            }

            if( !isset( $request->email ) || empty( $request->email ) ) {
                if( $fields == "" ) {
                    $fields .= "Email";
                } else {
                    $fields .= ", Email";
                }
            }

            if( !isset( $request->phone ) || empty( $request->phone ) ) {
                if( $fields == "" ) {
                    $fields .= "Phone";
                } else {
                    $fields .= ", Phone";
                }
            }

            if( !isset( $request->msg ) || empty( $request->msg ) ) {
                if( $fields == "" ) {
                    $fields .= "Message";
                } else {
                    $fields .= ", Message";
                }
            }
            $json_arr = array( "type" => "error", 'message' => 'Please fill ' . $fields . ' fields!' );
            return json_encode( $json_arr );
        }
        else {

            try{
                Mail::to(env('MAIL_SEND_ADDRESS'))->send(new ContactUsMail($request->name, $request->email, $request->phone, $request->msg));
                $json_arr = array( "type" => "success", "message" => 'Sent your information! Thank you for connecting with us' );
            }
            catch(Exception $e){
                logger($e);
                $json_arr = array( "type" => "error", "message" => 'Mail sending error. Please try again!' );
            }

            return json_encode( $json_arr );
        }
    }
    public function sendConversation(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'company_name' => 'nullable',
            'email' => 'required',
            'phone' => 'required',
            'msg' => 'required'
        ]);

        if ( $validator->fails() ) {
            $fields = "";
            if( !isset( $request->name ) || empty( $request->name ) ) {
                $fields .= "Name";
            }

            if( !isset( $request->company_name ) || empty( $request->company_name ) ) {
                if( $fields == "" ) {
                    $fields .= "Company Name";
                } else {
                    $fields .= ", Company Name";
                }
            }

            if( !isset( $request->email ) || empty( $request->email ) ) {
                if( $fields == "" ) {
                    $fields .= "Email";
                } else {
                    $fields .= ", Email";
                }
            }

            if( !isset( $request->phone ) || empty( $request->phone ) ) {
                if( $fields == "" ) {
                    $fields .= "Phone";
                } else {
                    $fields .= ", Phone";
                }
            }

            if( !isset( $request->msg ) || empty( $request->msg ) ) {
                if( $fields == "" ) {
                    $fields .= "Message";
                } else {
                    $fields .= ", Message";
                }
            }
            $json_arr = array( "type" => "error", 'message' => 'Please fill ' . $fields . ' fields!' );
            return json_encode( $json_arr );
        }
        else {
            try{
                Mail::to(env('MAIL_SEND_ADDRESS'))->send(new ConversationMail($request->name, $request->company_name, $request->email, $request->phone, $request->msg));
                $json_arr = array( "type" => "success", "message" => 'Sent your information! Thank you for creating a conversation with us' );
            }
            catch(Exception $e){
                logger($e);
                $json_arr = array( "type" => "error", "message" => 'Mail sending error. Please try again!' );
            }
            return json_encode( $json_arr );
        }
    }
}
