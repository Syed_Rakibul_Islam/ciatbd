<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset(auth()->user()->id)){
            if (auth()->user()->role == 'admin' || Auth::user()->role == 'super')
            {
                return $next($request);
            }
            return redirect(URL::previous());
        }
        return redirect('/admin/login');

    }
}
