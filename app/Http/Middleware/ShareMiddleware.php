<?php

namespace App\Http\Middleware;

use App\Post;
use Closure;

class ShareMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \View::share('posts', Post::where('type', '!=', 'blog')->where('status', 1)->select('slug', 'type', 'title')->get());

        return $next($request);
    }
}
