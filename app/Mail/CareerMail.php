<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CareerMail extends Mailable
{
    use Queueable, SerializesModels;
    public $name, $work_type, $work_field, $work_prefer, $message, $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $work_type, $work_field, $work_prefer, $message, $file)
    {
        $this->name = $name;
        $this->work_type = $work_type;
        $this->work_field = $work_field;
        $this->work_prefer = $work_prefer;
        $this->message = $message;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = env('MAIL_FROM_ADDRESS');
        $title = env('MAIL_FROM_NAME');
        return $this->markdown('mail.career')
            ->from($from, $title)
            ->replyTo($from, $title)
            ->subject('Career')
            ->with([
                'name' => $this->name,
                'work_type' => $this->work_type,
                'work_field' => $this->work_field,
                'work_prefer' => $this->work_prefer,
                'description' => $this->message,
                'file' => $this->file
            ]);
    }
}
