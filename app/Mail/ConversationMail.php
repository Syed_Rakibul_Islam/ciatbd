<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConversationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $name, $company_name, $email, $phone, $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $company_name, $email, $phone, $message)
    {
        $this->name = $name;
        $this->company_name = $company_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = env('MAIL_FROM_ADDRESS');
        $title = env('MAIL_FROM_NAME');
        return $this->markdown('mail.conversation')
            ->from($from, $title)
            ->replyTo($from, $title)
            ->subject('Conversation')
            ->with([
                'name' => $this->name,
                'company_name' => $this->company_name,
                'email' => $this->email,
                'phone' => $this->phone,
                'description' => $this->message
            ]);
    }
}
