<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'slug', 'type', 'title', 'description', 'image', 'video_url', 'status', 'post_date', 'is_video', 'user_id', 'options'
    ];

    public function user(){
        $this->belongsTo('App\User');
    }
    public function categories(){
        return $this->belongsToMany('App\Category','post_categories');
    }
}
