<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'post_id', 'category_id'
    ];

    public function post(){
        return $this->belongsTo('App\Post');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
}
