<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug', 150)->index()->nullable();
            $table->string('type', 50)->index()->nullable();
            $table->string('title', 150)->nullable();
            $table->longText('description')->nullable();
            $table->string('image', 500)->nullable();
            $table->string('video_url', 500)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->dateTime('post_date')->nullable();
            $table->tinyInteger('is_video')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
