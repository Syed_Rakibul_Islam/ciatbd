<?php

use Illuminate\Database\Seeder;
use App\Post;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isPost = Post::where(['type' => 'about', 'slug' => 'who-we-are'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'who-we-are',
                'type' => 'about',
                'title' => 'Who We Are',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
                'video_url' => 'https://www.youtube.com/embed/X4TSIh3RGoM',
                'user_id' => 1,
                'status' => 1,
                'is_video' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'about', 'slug' => 'what-we-do'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'what-we-do',
                'type' => 'about',
                'title' => 'What We Do',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
                'video_url' => 'https://www.youtube.com/embed/X4TSIh3RGoM',
                'user_id' => 1,
                'status' => 1,
                'is_video' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'about', 'slug' => 'why-ciat'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'why-ciat',
                'type' => 'about',
                'title' => 'Why CIAT',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
                'video_url' => 'https://www.youtube.com/embed/X4TSIh3RGoM',
                'user_id' => 1,
                'status' => 1,
                'is_video' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'about', 'slug' => 'meet-our-team'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'meet-our-team',
                'type' => 'about',
                'title' => 'Meet Our Team',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
                'video_url' => 'https://www.youtube.com/embed/X4TSIh3RGoM',
                'user_id' => 1,
                'status' => 1,
                'is_video' => 1
            ]);
        }
    }
}
