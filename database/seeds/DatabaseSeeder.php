<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MakeSuperAdminSeeder::class);
        $this->call(MakeAdminSeeder::class);
        $this->call(MakeUserSeeder::class);
        $this->call(AboutUsSeeder::class);
        $this->call(WhyChooseUsSeeder::class);
        $this->call(MakeInsightSeeder::class);
        $this->call(MakeContactSeeder::class);
    }
}
