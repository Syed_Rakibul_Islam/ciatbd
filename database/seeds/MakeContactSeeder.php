<?php

use Illuminate\Database\Seeder;
use App\Post;

class MakeContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isPost = Post::where(['type' => 'contact', 'slug' => 'contact'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'contact',
                'type' => 'contact',
                'title' => 'Contact Info',
                'description' => 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adi pisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
                'options' => json_encode(['address' => '379 5Th Ave New York, Nyc 10018', 'email' => 'contact@ciatbd.com', 'contact' => '
(+880) 123456789' ]),
                'user_id' => 1,
                'status' => 1
            ]);
        }
    }
}
