<?php

use Illuminate\Database\Seeder;
use App\Post;

class MakeInsightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isPost = Post::where(['type' => 'insight', 'slug' => 'data-statistics'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'data-statistics',
                'type' => 'insight',
                'title' => 'Data & Statistics',
                'description' => 'In this segment of work, we analyze given policies and possible policy adjustments to determine which parts are relevant to our clients.',
                'image' => 'arrow.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'insight', 'slug' => 'industry-insight'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'industry-insight',
                'type' => 'insight',
                'title' => 'INDUSTRY INSIGHT',
                'description' => 'In this segment of work, we analyze given policies and possible policy adjustments to determine which parts are relevant to our clients.',
                'image' => 'arrow.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'insight', 'slug' => 'portfolio'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'portfolio',
                'type' => 'insight',
                'title' => 'PORTFOLIO',
                'description' => 'In this segment of work, we analyze given policies and possible policy adjustments to determine which parts are relevant to our clients.',
                'image' => 'arrow.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'insight', 'slug' => 'blog'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'blog',
                'type' => 'insight',
                'title' => 'BLOG',
                'description' => 'In this segment of work, we analyze given policies and possible policy adjustments to determine which parts are relevant to our clients.',
                'image' => 'arrow.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'insight', 'slug' => 'workshop-and-interview'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'workshop-and-interview',
                'type' => 'insight',
                'title' => 'WORKSHOP AND INTERVIEW',
                'description' => 'In this segment of work, we analyze given policies and possible policy adjustments to determine which parts are relevant to our clients.',
                'image' => 'arrow.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
    }
}
