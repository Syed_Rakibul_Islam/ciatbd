<?php

use App\Post;
use Illuminate\Database\Seeder;

class WhyChooseUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isPost = Post::where(['type' => 'wcu', 'slug' => 'experienced-adviser'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'experienced-adviser',
                'type' => 'wcu',
                'title' => 'Experienced Adviser',
                'description' => 'Sed nec egestas diam, vitae imper-diet nisi. Vivamus cursus.',
                'image' => 'symbol-01-dark.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'wcu', 'slug' => 'great-ideas'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'great-ideas',
                'type' => 'wcu',
                'title' => 'Great Ideas',
                'description' => 'Sed nec egestas diam, vitae imper-diet nisi. Vivamus cursus.',
                'image' => 'symbol-02-dark.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'wcu', 'slug' => 'worldwide-system'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'worldwide-system',
                'type' => 'wcu',
                'title' => 'Worldwide System',
                'description' => 'Sed nec egestas diam, vitae imper-diet nisi. Vivamus cursus.',
                'image' => 'symbol-03-dark.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
        $isPost = Post::where(['type' => 'wcu', 'slug' => 'security-information'])->first();
        if(empty($isPost)) {
            Post::create([
                'slug' => 'security-information',
                'type' => 'wcu',
                'title' => 'Security Information',
                'description' => 'Sed nec egestas diam, vitae imper-diet nisi. Vivamus cursus.',
                'image' => 'symbol-04-dark.png',
                'user_id' => 1,
                'status' => 1
            ]);
        }
    }
}
