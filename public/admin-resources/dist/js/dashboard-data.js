/*Dashboard Init*/

var sparklineLogin = function() { 
	if( $('#sparkline_1').length > 0 ){
		$("#sparkline_1").sparkline([2,4,4,6,8,5,6,4,8,6,6,2 ], {
			type: 'line',
			width: '100%',
			height: '35',
			lineColor: '#177ec1',
			fillColor: 'rgba(23,126,193,.2)',
			maxSpotColor: '#177ec1',
			highlightLineColor: 'rgba(0, 0, 0, 0.2)',
			highlightSpotColor: '#177ec1'
		});
	}	
	if( $('#sparkline_2').length > 0 ){
		$("#sparkline_2").sparkline([0,2,8,6,8], {
			type: 'line',
			width: '100%',
			height: '35',
			lineColor: '#177ec1',
			fillColor: 'rgba(23,126,193,.2)',
			maxSpotColor: '#177ec1',
			highlightLineColor: 'rgba(0, 0, 0, 0.2)',
			highlightSpotColor: '#177ec1'
		});
	}	
	if( $('#sparkline_3').length > 0 ){
		$("#sparkline_3").sparkline([0, 23, 43, 35, 44, 45, 56, 37, 40, 45, 56, 7, 10], {
			type: 'line',
			width: '100%',
			height: '35',
			lineColor: '#177ec1',
			fillColor: 'rgba(23,126,193,.2)',
			maxSpotColor: '#177ec1',
			highlightLineColor: 'rgba(0, 0, 0, 0.2)',
			highlightSpotColor: '#177ec1'
		});
	}
	if( $('#sparkline_4').length > 0 ){
		$("#sparkline_4").sparkline([0,2,8,6,8,5,6,4,8,6,6,2 ], {
			type: 'bar',
			width: '100%',
			height: '50',
			barWidth: '5',
			resize: true,
			barSpacing: '5',
			barColor: '#e69a2a',
			highlightSpotColor: '#e69a2a'
		});
	}	
}
var sparkResize;
	$(window).resize(function(e) {
		clearTimeout(sparkResize);
		sparkResize = setTimeout(sparklineLogin, 200);
	});
sparklineLogin();