@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-05.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                About Us
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>

                <span class="breadcrumb-item">
					Contact Us
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <section class="bg-0 p-t-95 p-b-40">
        <div class="container" >
            @php($about = $items->where('type', 'about'))
            @foreach($about as $item)
            <div id="{{ $item->slug }}" style="margin-top: -75px; padding-top: 75px;">
                <h1 class="t1-b-1 text-center m-b-25 text-uppercase" >{{ $item->title }}</h1>
                <hr/>
                <div class="row justify-content-center">
                    <div class="col-sm-10 col-md-6 p-b-60">
                        <div class='embed-container'><iframe src='{{ $item->video_url }}' frameborder='0' allowfullscreen></iframe></div>
                    </div>
                    <div class="col-sm-10 col-md-6 p-b-60">
                        {!! $item->description !!}
                    </div>
                </div>
            </div>

            @endforeach
        </div>
    </section>

    <!-- Why chosse us -->
    <section class="bg-12 p-b-70">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    Why Chosse Us
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!--  -->
            <div class="row justify-content-center">
                @php($wcu = $items->where('type', 'wcu'))
                @foreach($wcu as $item)
                <div class="col-sm-10 col-md-6 col-lg-3 p-b-30">
                    <!-- Block1 -->
                    <div class="block1 wcu-block trans-04">
                        <div class="block1-show trans-04">
                            <div class="block1-symbol txt-center wrap-pic-max-s m-b-23 pos-relative lh-00 trans-04">
                                <img class="symbol-dark trans-04" src="{{ asset('images/upload/wcu/' . $item->image) }}" alt="IMG">
                                <img class="symbol-light ab-t-c op-00 trans-04" src="{{ asset('images/upload/wcu/' . $item->image) }}" alt="{{ $item->title }}">
                            </div>

                            <h4 class="block1-title t1-m-1 text-uppercase cl-3 txt-center trans-04">
                                {{ $item->title }}
                            </h4>
                        </div>

                        <div class="block1-hide flex-col-c-c p-t-8 trans-04">
                            <p class="t1-s-2 cl-12 txt-center p-b-26">
                                {!! $item->description !!}
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="bg-0 p-t-10 p-b-52">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    Meet Our Team
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!--  -->
            <div class="row justify-content-center">
                @php($teams = $items->where('type', 'team'))
                @foreach($teams as $item)
                <div class="col-sm-6 col-md-5 col-lg-3 p-b-40">
                    <div>
                        <div class="wrap-pic-w pos-relative">
                            <img src="{{ !empty($item->image) ? asset('images/upload/team/' . $item->image) : asset('images/team-01.jpg') }}" alt="{{ $item->title }}">

                            <div class="s-full ab-t-l flex-wr-c-c p-tb-30 hov-2">

                                <p class="hov-btn3 hov-2-1 text-white">{!! $item->description !!}</p>
                            </div>
                        </div>

                        <div class="flex-col-c-c p-t-28">
                            <a href="#" class="t1-m-1 text-uppercase cl-3 txt-center hov-link2 trans-02 m-b-5">
                                {{ $item->title }}
                            </a>

                            <span class="t1-s-5 cl-6 txt-center">
								{{ @json_decode($item->options)->designation }}
							</span>
                            <span class="t1-s-5 cl-6 txt-center">
								<a href="{{ @json_decode($item->options)->linkedin }}" target="_blank" class="btn btn-linkedin">Linkedin</a>
							</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
        (function ($) {

            // Select all links with hashes
            $('a[href*="#"]')
            // Remove links that don't actually link to anything
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function(event) {
                    // On-page links
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        &&
                        location.hostname == this.hostname
                    ) {
                        // Figure out element to scroll to
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            // event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top
                            }, 1000, function() {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                };
                            });
                        }
                    }
                });

        })(jQuery);

    </script>
@endsection
