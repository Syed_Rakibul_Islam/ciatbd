@extends('admin.layouts.app')

@section('title', ' About | ' . $post->slug . ' | Edit' )

@section('style')

    <!-- Bootstrap Wysihtml5 css -->
    <link rel="stylesheet" href="{{ asset('admin-resources/vendors/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css') }}" />
@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <form method="POST" action="{{ url('admin/about/' . $post->id) }}" data-toggle="validator">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                            <label for="title" class="control-label mb-10">Title *</label>
                                            <input type="text" name="title" placeholder="Enter name" class="form-control" maxlength="150" value="{{ old('title', $post->title) }}" required />
                                            {!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('video_url') ? 'has-error' : ''}}">
                                            <label for="video_url" class="control-label mb-10">Video URL</label>
                                            <input type="text" name="video_url" placeholder="Enter youtube embed video URL" class="form-control" value="{{ old('video_url', $post->video_url) }}" maxlength="255" />
                                            {!! $errors->first('video_url', '<p class="help-block text-danger">:message</p>') !!}
                                            <p class="help-block">You must add Youtube embed URL. Otherwise it can't work.</p>
                                        </div>
                                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                            <label for="description" class="control-label mb-10">Description</label>
                                            <textarea name="description" placeholder="Enter description..." class="form-control textarea_editor" maxlength="100000" > {!! old('description', $post->description)  !!} </textarea>
                                            {!! $errors->first('description', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>

                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

    <script>
        $(function() {

            "use strict";

            //editor
            $('.textarea_editor').wysihtml5({
                toolbar: {
                    fa: true,
                    "link": true
                }
            });

        });
    </script>
@endsection
