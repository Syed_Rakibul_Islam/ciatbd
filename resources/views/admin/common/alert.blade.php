@if (Session::has('flash_message'))
    <div class="alert alert-success alert-dismissable" style="margin: 0 -8px;" id="success-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">{{ Session::get('flash_message') }}</p>
        <div class="clearfix"></div>
    </div>
@elseif(Session::has('warning_msg'))
    <div class="alert alert-warning alert-dismissable" style="margin: 0 -8px;" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left">{{ Session::get('warning_msg') }}</p>
        <div class="clearfix"></div>
    </div>
@endif