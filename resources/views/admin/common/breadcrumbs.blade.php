<!-- Title -->
<div class="row heading-bg">
    <div class="col-sm-6 col-xs-12">
        <h5 class="txt-dark"> @yield('title') </h5>
    </div>
    <!-- Breadcrumb -->
    <div class="col-md-6 col-xs-12">
        <ol class="breadcrumb">
            <li>
                <i class="zmdi zmdi-home"></i>
                <a href="{{ url('/') }}">Home</a>
            </li>
            @foreach(Request::segments() as $segment)
                @if(count(Request::segments()) == $loop->iteration)
                    <li class="active" style="text-transform: capitalize;">
                        {{ str_replace('_', ' ', Request::segment($loop->iteration)) }}
                    </li>
                @else
                    <li>
                        <a style="text-transform: capitalize;" href="{{ url('/') }}@for($i = 1; $i <= $loop->iteration; $i++)/{{Request::segment($i)}}@endfor">
                            {{ str_replace('_', ' ', Request::segment($loop->iteration)) }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ol>
    </div>
    <!-- /Breadcrumb -->
</div>
<!-- /Title -->
