<footer class="footer container-fluid pl-30 pr-30">
    <div class="row">
        <div class="col-sm-12">
            <p>Copyright © CIATBD {{ date('Y') }}. All rights reserved. Developed by <a href="https://www.rizensolutions.com/" target="_blank">Rizen Solutions</a></p>
        </div>
    </div>
</footer>
