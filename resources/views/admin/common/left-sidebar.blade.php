<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li>
            <a class="{{ request()->is('admin') || request()->is('admin/dashboard') ? 'active' : '' }}" href="{{ url('/admin/dashboard') }}"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li class="navigation-header">
            <span>Home</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin/slider*') ? 'active' : '' }}" href="{{ url('admin/slider') }}"><div class="pull-left"><i class="zmdi zmdi-collection-folder-image mr-20"></i><span class="right-nav-text">Slider</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/testimonial*') ? 'active' : '' }}" href="{{ url('admin/testimonial') }}"><div class="pull-left"><i class="zmdi zmdi-quote mr-20"></i><span class="right-nav-text">Testimonial</span></div><div class="clearfix"></div></a>
        </li>
        <li class="navigation-header">
            <span>About</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin/about*') ? 'active' : '' }}" href="{{ url('admin/about') }}"><div class="pull-left"><i class="zmdi zmdi-file mr-20"></i><span class="right-nav-text">About</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/why-choose*') ? 'active' : '' }}" href="{{ url('admin/why-choose') }}"><div class="pull-left"><i class="zmdi zmdi-accounts-outline mr-20"></i><span class="right-nav-text">Why Choose</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/team*') ? 'active' : '' }}" href="{{ url('admin/team') }}"><div class="pull-left"><i class="zmdi zmdi-accounts mr-20"></i><span class="right-nav-text">Team</span></div><div class="clearfix"></div></a>
        </li>
        <li class="navigation-header">
            <span>Solutions</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin/solution*') ? 'active' : '' }}" href="{{ url('admin/solution') }}"><div class="pull-left"><i class="zmdi zmdi-wrench mr-20"></i><span class="right-nav-text">Solution</span></div><div class="clearfix"></div></a>
        </li>
        <li class="navigation-header">
            <span>Insight</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin/insight*') ? 'active' : '' }}" href="{{ url('admin/insight') }}"><div class="pull-left"><i class="zmdi zmdi-lamp mr-20"></i><span class="right-nav-text">Insight</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/data-statistics*') ? 'active' : '' }}" href="{{ url('admin/data-statistics') }}"><div class="pull-left"><i class="zmdi zmdi-chart mr-20"></i><span class="right-nav-text">Data & Statistics</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/industry-insight*') ? 'active' : '' }}" href="{{ url('admin/industry-insight') }}"><div class="pull-left"><i class="zmdi zmdi-store mr-20"></i><span class="right-nav-text">Industry Insight</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/portfolio*') ? 'active' : '' }}" href="{{ url('admin/portfolio') }}"><div class="pull-left"><i class="zmdi zmdi-case mr-20"></i><span class="right-nav-text">Portfolio</span></div><div class="clearfix"></div></a>
        </li>

        <li>
            <a class="{{ request()->is('admin/blog*') ? 'active' : '' }}" href="{{ url('admin/blog') }}"><div class="pull-left"><i class="zmdi zmdi-comment mr-20"></i><span class="right-nav-text">Blog</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/workshop-and-interview*') ? 'active' : '' }}" href="{{ url('admin/workshop-and-interview') }}"><div class="pull-left"><i class="zmdi zmdi-account-calendar mr-20"></i><span class="right-nav-text">Workshop and Interview</span></div><div class="clearfix"></div></a>
        </li>
        <li class="navigation-header">
            <span>Others</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin/client*') ? 'active' : '' }}" href="{{ url('admin/client') }}"><div class="pull-left"><i class="zmdi zmdi-male-female mr-20"></i><span class="right-nav-text">Client</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/contact*') ? 'active' : '' }}" href="{{ url('admin/contact') }}"><div class="pull-left"><i class="zmdi zmdi-account-box-phone mr-20"></i><span class="right-nav-text">Contact</span></div><div class="clearfix"></div></a>
        </li>
    </ul>
</div>
