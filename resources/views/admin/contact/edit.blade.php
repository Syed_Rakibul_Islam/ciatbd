@extends('admin.layouts.app')

@section('title', ' Contact | Edit' )

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <form method="POST" action="{{ url('admin/contact') }}" data-toggle="validator" >
                                        @csrf
                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                            <label for="title" class="control-label mb-10">Title *</label>
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" maxlength="150" value="{{ old('title', $post->title) }}" required />
                                            {!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                            <label for="description" class="control-label mb-10">Description</label>
                                            <textarea name="description" placeholder="Enter description..." class="form-control" maxlength="100000" > {!! old('description', $post->description) !!} </textarea>
                                            {!! $errors->first('description', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                            <label for="address" class="control-label mb-10">Address</label>
                                            <textarea name="address" placeholder="Enter address..." class="form-control" maxlength="500" > {!! old('address', @json_decode(@$post->options)->address) !!} </textarea>
                                            {!! $errors->first('address', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                            <label for="email" class="control-label mb-10">Email</label>
                                            <input type="email" name="email" placeholder="Enter email" class="form-control" maxlength="150" value="{{ old('email', @json_decode(@$post->options)->email) }}" />
                                            {!! $errors->first('email', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                            <label for="contact" class="control-label mb-10">Contact no</label>
                                            <input type="text" name="contact" placeholder="Enter contact no.." class="form-control" maxlength="150" value="{{ old('contact', @json_decode(@$post->options)->contact) }}" />
                                            {!! $errors->first('contact', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

@endsection
