@extends('admin.layouts.app')

@section('title', ' Data & Statistics | Sorting' )

@section('style')

    <!-- select2 CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('admin/data-statistics/sorting') }}" data-toggle="validator">
                                        @csrf
                                        <div class="form-group {{ $errors->has('options') ? 'has-error' : ''}}">
                                            <label for="options" class="control-label mb-10">Name *</label>
                                            <select name="options[]" class="form-control selectpicker" id="options" data-style="form-control btn-default btn-outline" required multiple>
                                                @php
                                                    if(!empty($options)){
                                                        foreach($options as $key => $value ){
                                                            foreach($posts as $item){
                                                                if((int)$value == $item->id){
                                                                    echo '<option value="' . $item->id . '" selected>' . $item->title . '</option>';
                                                                }
                                                            }
                                                        }
                                                        foreach($posts as $item){
                                                            if(!in_array($item->id, $options)){
                                                                echo '<option value="' . $item->id . '">' . $item->title . '</option>';
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        foreach($posts as $item){
                                                            echo '<option value="' . $item->id . '">' . $item->title . '</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                            {!! $errors->first('options', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection


@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function() {

            "use strict";

            $('#options').select2({
                placeholder: "Choose post categories",
                allowClear: true,
                multiple:true
            });

            $("#options").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                console.log(evt);

                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });

        });

    </script>
@endsection
