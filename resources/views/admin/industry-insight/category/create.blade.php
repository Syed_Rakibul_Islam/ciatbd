@extends('admin.layouts.app')

@section('title', 'Industry Insight | ' . $post->slug . ' | Category | Create' )

@section('style')

    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('admin/industry-insight/' . $post->id . '/category') }}" data-toggle="validator" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                            <label for="title" class="control-label mb-10">Title *</label>
                                            <input type="text" name="title" placeholder="Enter name" class="form-control" maxlength="150" value="{{ old('title') }}" required />
                                            {!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
                                            <label for="feature_image" class="control-label mb-10">Feature Image *</label>
                                            <input type="file" name="feature_image" class="form-control dropify" data-min-width="500" data-min-height="300" data-max-width="1000" data-max-height="600" required />
                                            <span class="text-info">NB: Image size must be grater than 500x300px and less than 1000x600px.</span>
                                            {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
                                            <label for="url" class="control-label mb-10">File URL *</label>
                                            <input type="text" name="url" placeholder="Enter file URL" class="form-control" maxlength="250" value="{{ old('url') }}" required />
                                            {!! $errors->first('url', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>

                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection


@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection
