@extends('admin.layouts.app')

@section('title', ' My Profile | Change Password' )

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('admin/profile/change-password') }}" data-toggle="validator">
                                        @csrf
                                        <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
                                            <label for="old_password" class="control-label mb-10">Old Password *</label>
                                            <input type="password" name="old_password" placeholder="Enter old password" class="form-control" maxlength="255" value="{{ old('password') }}" required />
                                            {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                            <label for="password" class="control-label mb-10">New Password *</label>
                                            <input type="password" name="password" placeholder="Enter new password" class="form-control" maxlength="255" value="{{ old('password') }}" required />
                                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                            <label for="password_confirmation" class="control-label mb-10">Confirm Password *</label>
                                            <input type="password" name="password_confirmation" placeholder="Enter confirm password" class="form-control" maxlength="255" value="{{ old('password_confirmation') }}" required />
                                            {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";


            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify');
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection
