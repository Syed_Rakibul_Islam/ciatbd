@extends('admin.layouts.app')

@section('title', ' My Profile | Edit' )

@section('style')

    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('admin/profile/edit') }}" data-toggle="validator" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                            <label for="name" class="control-label mb-10">Name *</label>
                                            <input type="text" name="name" placeholder="Enter name" class="form-control" maxlength="100" value="{{ old('name', @$user->name) }}" required />
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                            <label for="contact" class="control-label mb-10">Contact No</label>
                                            <input type="text" name="contact" placeholder="Enter contact no" class="form-control" maxlength="100" value="{{ old('contact', @$user->contact) }}" />
                                            {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('user_image') ? 'has-error' : ''}}">
                                            <label for="user_image" class="control-label mb-10">Profile image</label>
                                            @if(isset($user) && !empty($user->image) && file_exists('images/upload/user/' . $user->image))
                                                <input type="file" name="user_image" class="form-control dropify" maxlength="100" data-default-file="{{ asset('images/upload/user/' . $user->image) }}" data-max-file-size="300K"  />
                                            @else
                                                <input type="file" name="user_image" class="form-control dropify" maxlength="100" data-max-file-size="300K" />
                                            @endif
                                            <p class="text-primary">Image size must be less than 300KB.</p>
                                            {!! $errors->first('user_image', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <p class="text-info">NB: Image size must be less than 300KB.</p>
                                        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                            <label for="address" class="control-label mb-10">Address</label>
                                            <input type="text" name="address" placeholder="Enter address" class="form-control" maxlength="255" value="{{ old('address', @$user->address) }}" />
                                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                                            <label for="about" class="control-label mb-10">About me</label>
                                            <textarea name="about" class="form-control textarea_editor" maxlength="=100000" placeholder="Enter about me">{{ old('address', @$user->about) }}</textarea>
                                            {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";


            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify');
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection
