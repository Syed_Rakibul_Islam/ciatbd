@extends('admin.layouts.app')

@section('title', ' Slider | Create' )

@section('style')

    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('admin/slider') }}" data-toggle="validator" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                            <label for="title" class="control-label mb-10">Title *</label>
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" maxlength="150" value="{{ old('title') }}" required />
                                            {!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                            <label for="description" class="control-label mb-10">Text *</label>
                                            <input type="text" name="description" placeholder="Enter ..." class="form-control" maxlength="250" value="{{ old('description') }}" required />
                                            {!! $errors->first('description', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
                                            <label for="feature_image" class="control-label mb-10">Feature Image *</label>
                                            <input type="file" name="feature_image" class="form-control dropify" data-min-width="1919" data-min-height="499" data-max-width="1921" data-max-height="501" />
                                            <p class="text-primary">Image size must be 1920X500px.</p>
                                            {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('button_1_name') ? 'has-error' : ''}}">
                                            <label for="button_1_name" class="control-label mb-10">Button 1 Name *</label>
                                            <input type="text" name="button_1_name" placeholder="Enter button 1 name" class="form-control" maxlength="50" value="{{ old('button_1_name') }}" required />
                                            {!! $errors->first('button_1_name', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('button_1_url') ? 'has-error' : ''}}">
                                            <label for="button_1_url" class="control-label mb-10">Button 1 URL *</label>
                                            <input type="text" name="button_1_url" placeholder="Enter button 1 name" class="form-control" maxlength="500" value="{{ old('button_1_url') }}" required />
                                            {!! $errors->first('button_1_url', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('button_2_name') ? 'has-error' : ''}}">
                                            <label for="button_2_name" class="control-label mb-10">Button 2 Name *</label>
                                            <input type="text" name="button_2_name" placeholder="Enter button 2 name" class="form-control" maxlength="50" value="{{ old('button_2_name') }}" required />
                                            {!! $errors->first('button_2_name', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('button_2_url') ? 'has-error' : ''}}">
                                            <label for="button_2_url" class="control-label mb-10">Button 2 URL *</label>
                                            <input type="text" name="button_2_url" placeholder="Enter button 2 name" class="form-control" maxlength="500" value="{{ old('button_2_url') }}" required />
                                            {!! $errors->first('button_2_url', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>


                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection


@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";


            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection
