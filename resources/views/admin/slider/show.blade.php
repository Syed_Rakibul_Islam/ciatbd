@extends('admin.layouts.app')

@section('title', ' Slider | View | ' . $post->slug )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('/admin/slider') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        <a href="{{ url('/admin/slider/' . $post->id . '/edit') }}" class="btn btn-primary pa-5" title="Edit"><i class="zmdi zmdi-edit txt-light"></i></a>
                        <form method="POST" action="{{ url('admin/slider/' . $post->id) }}" class="inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger pa-5 btn-sm" title="Delete" onclick="return confirm('Confirm delete?')">
                                <i class="zmdi zmdi-delete txt-light"></i>
                            </button>
                        </form>
                        @if($post->status == 1)
                            <form method="POST" action="{{ url('admin/slider/' . $post->id . '/unpublish') }}" class="inline-block">
                                @csrf
                                <button type="submit" class="btn btn-warning pa-5 btn-sm" title="Unpublish" onclick="return confirm('Confirm unpublish?')">
                                    <i class="zmdi zmdi-circle-o txt-light"></i>
                                </button>
                            </form>
                        @else
                            <form method="POST" action="{{ url('admin/slider/' . $post->id . '/publish') }}" class="inline-block">
                                @csrf
                                <button type="submit" class="btn btn-success pa-5 btn-sm" title="Publish" onclick="return confirm('Confirm publish?')">
                                    <i class="zmdi zmdi-check-circle txt-light"></i>
                                </button>
                            </form>
                        @endif
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $post->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Slug </th>
                                            <td> {{ $post->slug }} </td>
                                        </tr>
                                        <tr>
                                            <th> Title </th>
                                            <td> {{ $post->title }} </td>
                                        </tr>
                                        <tr>
                                            <th> Text </th>
                                            <td> {!! $post->description !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Feature Image </th>
                                            <td><img src="{{ asset('images/upload/slider/' . $post->image) }}" class="img-responsive"/> </td>
                                        </tr>
                                        @php($options = @json_decode($post->options))
                                        <tr>
                                            <th> Button 1 Text </th>
                                            <td> {{ @$options->button_1_name }} </td>
                                        </tr>
                                        <tr>
                                            <th> Button 1 URL </th>
                                            <td> {{ @$options->button_1_url }} </td>
                                        </tr>
                                        <tr>
                                            <th> Button 2 Text </th>
                                            <td> {{ @$options->button_2_name }} </td>
                                        </tr>
                                        <tr>
                                            <th> Button 2 URL </th>
                                            <td> {{ @$options->button_2_url }} </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#data-table').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
