@extends('admin.layouts.app')

@section('title', ' Solution | ' . $post->slug . ' | Category | View | ' . $category->id  )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('admin/solution/' . $post->id . '/category/') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        <a href="{{ url('admin/solution/' . $post->id . '/category/' . $category->id . '/edit') }}" class="btn btn-primary pa-5" title="Edit"><i class="zmdi zmdi-edit txt-light"></i></a>
                        <form method="POST" action="{{ url('admin/solution/' . $post->id) }}" class="inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger pa-5 btn-sm" title="Delete" onclick="return confirm('Confirm delete?')">
                                <i class="zmdi zmdi-delete txt-light"></i>
                            </button>
                        </form>
                        @if($category->status == 1)
                            <form method="POST" action="{{ url('admin/solution/' . $post->id . '/category/' . $category->id . '/unpublish') }}" class="inline-block">
                                @csrf
                                <button type="submit" class="btn btn-warning pa-5 btn-sm" title="Unpublish" onclick="return confirm('Confirm unpublish?')">
                                    <i class="zmdi zmdi-circle-o txt-light"></i>
                                </button>
                            </form>
                        @else
                            <form method="POST" action="{{ url('admin/solution/' . $post->id . '/category/' . $category->id . '/publish') }}" class="inline-block">
                                @csrf
                                <button type="submit" class="btn btn-success pa-5 btn-sm" title="Publish" onclick="return confirm('Confirm publish?')">
                                    <i class="zmdi zmdi-check-circle txt-light"></i>
                                </button>
                            </form>
                        @endif
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $category->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Slug </th>
                                            <td> {{ $category->slug }} </td>
                                        </tr>
                                        <tr>
                                            <th> Title </th>
                                            <td> {{ $category->title }} </td>
                                        </tr>
                                        <tr>
                                            <th> Solution Title </th>
                                            <td> {!! $post->title !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Solution Type </th>
                                            <td> {{ @ucwords(@str_replace('_', ' ', @json_decode($post->options)->solution_type)) }} </td>
                                        </tr>
                                        <tr>
                                            <th> Description </th>
                                            <td> {!! $category->description !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Video URL </th>
                                            <td> {!! $post->video_url !!} </td>
                                        </tr>
                                        @if(!empty($category->image))
                                        <tr>
                                            <th> Image </th>
                                            <td><img src="{{ asset('images/upload/solution/category/' . $category->image) }}" alt="{{ $category->title }}" class="img-responsive"/> </td>

                                        </tr>
                                        @endif

                                        <tr>
                                            <th> Status </th>
                                            @if($category->status == 1)
                                                <td class="text-success">Published</td>
                                            @else
                                                <td class="text-danger">Unpublished</td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#data-table').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
