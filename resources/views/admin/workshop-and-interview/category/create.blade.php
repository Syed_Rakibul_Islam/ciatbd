@extends('admin.layouts.app')

@section('title', 'Workshop and Interview | ' . $post->slug . ' | Category | Create' )

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('admin/workshop-and-interview/' . $post->id . '/category') }}" data-toggle="validator">
                                        @csrf
                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                            <label for="title" class="control-label mb-10">Title *</label>
                                            <input type="text" name="title" placeholder="Enter name" class="form-control" maxlength="150" value="{{ old('title') }}" required />
                                            {!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('video_url') ? 'has-error' : ''}}">
                                            <label for="video_url" class="control-label mb-10">Video URL *</label>
                                            <input type="text" name="video_url" placeholder="Enter video url" class="form-control" maxlength="500" value="{{ old('video_url') }}" required />
                                            <span class="text-info">NB: You have to provide youtube embed URL.</span>
                                            {!! $errors->first('video_url', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>

                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection


@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

@endsection
