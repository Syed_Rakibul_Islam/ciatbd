@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-08.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-3 text-uppercase cl-0 txt-center m-b-25">
                WORK IS MORE IMPORTANT THAN JUST PRESENCE
            </h2>
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Career
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>

                <span class="breadcrumb-item">
					Career
				</span>
            </div>
        </div>
    </section>

    <!-- Contact -->
    <section class="bg-0 p-t-95 p-b-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 p-b-60">
                    <h3 class="t1-m-5 cl-3 m-b-44">
                        Send Us your Information
                    </h3>

                    @if ($errors->any())
                        <div class="alert alert-danger" align="center" id="error-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul style="list-style-type: none;">
                                @foreach ($errors->all() as $error)
                                    <li >{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form id="career-form" class="validate-form" method="post" action="{{ url('career') }}" name="contact" enctype="multipart/form-data">
                        @csrf
                        <div class="m-b-15 validate-input" data-validate = "Name is required">
                            <input class="size-a-3 t1-m-2 plh-6 cl-6 p-rl-20 bo-1-rad-4 bcl-12 focus-in1" type="text" name="name" placeholder="Your Name *" required>
                        </div>

                        <div class="m-b-15 validate-input" data-validate = "Work Tpe is required">
                            <select class="size-a-3 t1-m-2 plh-6 cl-6 p-rl-20 bo-1-rad-4 bcl-12 focus-in1" name="work_type" aria-hidden="true" required>
                                <option value="">Select work type *</option>
                                <option value="part_time">Part Time</option>
                                <option value="full_time">Fill Time</option>
                            </select>
                        </div>

                        <div class="m-b-15 validate-input" >
                            <select class="size-a-3 t1-m-2 plh-6 cl-6 p-rl-20 bo-1-rad-4 bcl-12 focus-in1" name="work_field" aria-hidden="true" required>
                                <option value="">Which field are you looking for? *</option>
                                <option value="marketing">Marketing</option>
                                <option value="research">Research/Consultancy</option>
                            </select>
                        </div>

                        <div class="m-b-15 validate-input" >
                            <select class="size-a-3 t1-m-2 plh-6 cl-6 p-rl-20 bo-1-rad-4 bcl-12 focus-in1" name="work_prefer" aria-hidden="true" required>
                                <option value="">Which would prefer? *</option>
                                <option value="home">Working from home</option>
                                <option value="office">In office work</option>
                            </select>
                        </div>
                        <div class="m-b-30 validate-input">
                            <textarea class="size-a-14 t1-m-2 plh-6 cl-6 p-rl-20 p-tb-13 bo-1-rad-4 bcl-12 focus-in1" name="msg" placeholder="Your Message *" required></textarea>
                        </div>
                        <div class="m-b-15 validate-input" data-validate = "Name is required">
                            <label>Upload your resume *</label>
                            <input class="size-a-3 t1-m-2 plh-6 cl-6 p-rl-20 bo-1-rad-4 bcl-12 focus-in1" type="file" name="file" required>
                        </div>

                        <button class="size-a-15 flex-c-c bg-11 t1-s-2 text-uppercase cl-0 hov-btn1 trans-02 p-rl-15">
                            Send Email
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    @if (Session::has('flash_message'))
        <script>
            swal("", '{{ Session::get('flash_message') }}', 'success');
        </script>
    @endif
@endsection
