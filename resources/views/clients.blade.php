@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-05.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Clients
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>

                <span class="breadcrumb-item">
					Clients
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <section class="bg-0 p-t-30 p-b-40">
        <div class="container" >
            <div class="row justify-content-center">
                @foreach($items as $item)
                    <div class="col-xs-6 col-sm-3 col-lg-3 p-b-45 isotope-item business-n">
                        <div class="flex-col-s-c">
                            <div class="w-full pos-relative wrap-pic-w m-b-16">
                                <img class="img-responsive" src="{{ !empty($item->image) ? asset('images/upload/client/' . $item->image) : asset('images/project-04.jpg') }}" alt="{{ $item->title }}">

                                <div class="s-full ab-t-l flex-col-c-c hov-1 p-tb-30 p-rl-15">
                                    <a href="{{ url('clients/' . $item->slug) }}" class="size-a-15 d-inline-flex flex-c-c bg-11 t1-s-2 text-uppercase cl-0 hov-btn2 trans-02 p-rl-10 hov-1-2">
                                        View Details
                                    </a>
                                </div>
                            </div>

                            <a href="projects-detail-01.html" class="t1-m-1 cl-3 hov-link2 trans-02">
                                {{ $item->title }}
                            </a>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </section>

@endsection
