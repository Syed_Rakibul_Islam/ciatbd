<footer>
    <div class="parallax100 kit-overlay1 p-t-35 p-b-10" style="background-image: url({{ asset('images/bg-03.jpg') }});">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start">
                <div class="col-md-6 p-b-20">
                    <div class="size-h-1 flex-s-e p-b-6 m-b-18">
                        <a href="#">
                            <img class="max-s-full" src="{{ asset('images/icons/logo-w.png') }}" alt="{{ config('app.name') }}">
                        </a>
                    </div>

                    <div>
                        <p class="t1-s-2 cl-13 p-b-17">
                            Center For Innovation & Alternative Thinking
                        </p>

                        <div class="flex-wr-s-c p-t-10">
                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10 btn-facebook">
                                <i class="fa fa-facebook"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10 btn-twitter">
                                <i class="fa fa-twitter"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10 btn-linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10 btn-instagram">
                                <i class="fa fa-instagram"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10 btn-youtube">
                                <i class="fa fa-youtube-play"></i>
                            </a>


                        </div>
                    </div>
                </div>

                <div class="col-md-6 p-b-20">
                    <div class="pull-right">
                        <div class="size-h-1 flex-s-e m-b-18">
                            <h4 class="t1-m-3 text-uppercase cl-0">
                                Contact us
                            </h4>
                        </div>

                        <ul>
                            <li class="flex-wr-s-s t1-s-2 cl-13 p-b-9">
                                    <span class="size-w-3">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                    </span>

                                <span class="size-w-4">
                                        379 5Th Ave New York, Nyc 10018
                                    </span>
                            </li>

                            <li class="flex-wr-s-s t1-s-2 cl-13 p-b-9">
                                    <span class="size-w-3">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    </span>

                                <span class="size-w-4">
                                        contac@gmail.com
                                    </span>
                            </li>

                            <li class="flex-wr-s-s t1-s-2 cl-13 p-b-9">
                                    <span class="size-w-3">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </span>

                                <span class="size-w-4">
                                        (+1) 96 716 6879
                                        <br>
                                        (+1) 96 716 6897
                                    </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-10">
        <div class="container txt-center p-tb-15">
				<span class="t1-s-2 cl-14">
					Copyright © CIATBD {{ date('Y') }}. All rights reserved. Developed by <a href="https://www.rizensolutions.com/" target="_blank">Rizen Solutions</a>
				</span>
        </div>
    </div>
</footer>
