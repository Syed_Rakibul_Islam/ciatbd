@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-07.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Data & Statistics
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>
                <a href="{{ url('/insights') }}" class="breadcrumb-item">
                    Insights
                </a>

                <span class="breadcrumb-item">
					Data & Statistics
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <section class="bg-0 p-t-30 p-b-40">
        <div class="container" >
            <div class="row justify-content-center">
                @foreach($items as $item)
                <div class="col-md-6 col-lg-4 p-b-50">
                    <div class="bg-0 h-full">

                        <div class="bg-0 p-rl-28 p-t-26 p-b-35">
                            <h4 class="p-b-12">
                                <a href="{{ url('insights/data-statistics/' . $item->slug) }}" class="t1-m-1 cl-3 hov-link2 trans-02">
                                    {{ $item->title }}
                                </a>
                            </h4>

                            <p class="t1-s-2 cl-6 p-b-20">
                                {!! str_limit($item->description, 300, $end='...') !!}
                            </p>

                            <a href="{{ url('insights/data-statistics/' . $item->slug) }}" class="d-inline-flex flex-c-c size-a-1 p-rl-15 t1-s-2 text-uppercase cl-0 bg-11 hov-btn1 trans-02">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection
