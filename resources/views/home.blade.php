@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/css/navigation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/css/settings.css') }}">
@endsection

@section('content')
    <!-- Slide -->
    <section class="slider">
        <div class="rev_slider_wrapper fullwidthbanner-container">
            <div id="rev_slider_1" class="rev_slider fullwidthabanner" data-version="5.4.5" style="display:none">
                <ul>
                    <!-- Slide 1 -->

                    <?php
                    $itemOptions = $options->where('name', 'slider')->first();
                    if(!empty($itemOptions->options)):
                        $optionsArray = json_decode($itemOptions->options);
                        foreach($optionsArray as $option):
                            $item = $items->where('id', $option)->first();
                            if(!empty($item)): ?>

                                <li data-transition="slidingoverlayhorizontal">
                                    <img src="{{ !empty($item->image) ? asset('images/upload/slider/' .  $item->image) : asset('images/slide-01.jpg') }}" alt="{{ $item->title }}" class="rev-slidebg">

                                    <h2 class="tp-caption tp-resizeme caption-1 text-uppercase"
                                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-visibility="['on', 'on', 'on', 'on']"

                                        data-fontsize="['48', '48', '48', '38']"
                                        data-lineheight="['58', '58', '58', '58']"
                                        data-color="['#FFF']"
                                        data-textAlign="['center', 'center', 'center', 'center']"

                                        data-x="['center']"
                                        data-y="['center']"
                                        data-hoffset="['0', '0', '0', '0']"
                                        data-voffset="['-83', '-83', '-83', '-93']"

                                        data-width="['1200','992','768','480']"
                                        data-height="['auto', 'auto', 'auto', 'auto']"
                                        data-whitespace="['normal']"

                                        data-paddingtop="[0, 0, 0, 0]"
                                        data-paddingright="[15, 15, 15, 15]"
                                        data-paddingbottom="[0, 0, 0, 0]"
                                        data-paddingleft="[15, 15, 15, 15]"

                                        data-basealign="slide"
                                        data-responsive_offset="off"
                                    >{{ $item->title }}</h2>

                                    <p class="tp-caption tp-resizeme caption-2"
                                       data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                       data-visibility="['on', 'on', 'on', 'on']"

                                       data-fontsize="['30', '30', '30', '25']"
                                       data-lineheight="['39', '39', '39', '39']"
                                       data-color="['#FFF']"
                                       data-textAlign="['center', 'center', 'center', 'center']"

                                       data-x="['center']"
                                       data-y="['center']"
                                       data-hoffset="['0', '0', '0', '0']"
                                       data-voffset="['-13', '-13', '-13', '-13']"

                                       data-width="['1200','992','768','480']"
                                       data-height="['auto', 'auto', 'auto', 'auto']"
                                       data-whitespace="['normal']"

                                       data-paddingtop="[0, 0, 0, 0]"
                                       data-paddingright="[15, 15, 15, 15]"
                                       data-paddingbottom="[0, 0, 0, 0]"
                                       data-paddingleft="[15, 15, 15, 15]"

                                       data-basealign="slide"
                                       data-responsive_offset="off"
                                    >
                                        {!! $item->description !!}
                                    </p>

                                    <div class="tp-caption tp-resizeme caption-3 flex-wr-c-c d-flex"
                                         data-frames='[{"delay":3000,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                         data-x="['center']"
                                         data-y="['center']"
                                         data-hoffset="['0', '0', '0', '0']"
                                         data-voffset="['88', '88', '88', '88']"

                                         data-width="['1200','992','768','480']"
                                         data-height="['auto']"

                                         data-paddingtop="[0, 0, 0, 0]"
                                         data-paddingright="[10, 10, 10, 10]"
                                         data-paddingbottom="[0, 0, 0, 0]"
                                         data-paddingleft="[10, 10, 10, 10]"

                                         data-basealign="slide"
                                         data-responsive_offset="off"
                                    >
                                        @php($sliderOptions = @json_decode($item->options))
                                        <a href="{{ @$sliderOptions->button_1_url }}" class="btn1 flex-c-c" target="_blank">
                                            {{ @$sliderOptions->button_1_name }}
                                        </a>

                                        <a href="{{ @$sliderOptions->button_2_url }}" class="btn2 flex-c-c" target="_blank">
                                            {{ @$sliderOptions->button_2_name }}
                                        </a>
                                    </div>
                                </li>
                            <?php endif;
                        endforeach;
                    endif; ?>

                </ul>
            </div>
        </div>
    </section>

    <!-- Services -->
    <section class="bg-0 p-t-92 p-b-60">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    OUR CAPABILITIES
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!--  -->
            <div class="row justify-content-center">
                <?php
                $itemOptions = $options->where('name', 'solution')->first();
                if(!empty($itemOptions->options)):
                    $optionsArray = json_decode($itemOptions->options);
                    foreach($optionsArray as $option):
                        $item = $items->where('id', $option)->first();
                        if(!empty($item)): ?>
                            <div class="col-sm-10 col-md-8 col-lg-4 p-b-40">
                                <div class="bg-10 h-full">
                                    <a href="{{ url('solutions/' . $item->id) }}" class="hov-img0 of-hidden bg-0">
                                        <img src="{{ !empty($item->image) ? asset('images/solution/' . $item->image) : asset('images/services-01.jpg')  }}" alt="{{ $item->title }}">
                                    </a>

                                    <div class="p-rl-30 p-t-26 p-b-20">
                                        <h4 class="p-b-9">
                                            <a href="{{ url('solutions/' . $item->slug) }}" class="t1-m-1 cl-0 hov-link2 trans-02">
                                                {{ $item->title }}
                                            </a>
                                        </h4>

                                        <p class="t1-s-2 cl-13">
                                            {!! str_limit($item->description, 100, $end='...') !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                    <?php endif;
                    endforeach;
                endif; ?>
            </div>
        </div>
    </section>

    <!-- Why chosse us -->
    <section class="bg-12 p-t-92 p-b-70">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    WHY CHOOSE US
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!--  -->
            <div class="row justify-content-center">
                <?php
                $itemOptions = $options->where('name', 'wcu')->first();
                if(!empty($itemOptions->options)):
                    $optionsArray = json_decode($itemOptions->options);
                    foreach($optionsArray as $option):
                        $item = $items->where('id', $option)->first();
                        if(!empty($item)): ?>
                            <div class="col-sm-10 col-md-6 col-lg-3 p-b-30">
                                <!-- Block1 -->
                                <div class="block1 wcu-block trans-04">
                                    <div class="block1-show trans-04">
                                        <div class="block1-symbol txt-center wrap-pic-max-s m-b-23 pos-relative lh-00 trans-04">
                                            <img class="symbol-dark trans-04" src="{{ asset('images/upload/wcu/' . $item->image) }}" alt="IMG">
                                            <img class="symbol-light ab-t-c op-00 trans-04" src="{{ asset('images/upload/wcu/' . $item->image) }}" alt="{{ $item->title }}">
                                        </div>

                                        <h4 class="block1-title t1-m-1 text-uppercase cl-3 txt-center trans-04">
                                            {{ $item->title }}
                                        </h4>
                                    </div>

                                    <div class="block1-hide flex-col-c-c p-t-8 trans-04">
                                        <p class="t1-s-2 cl-12 txt-center p-b-26">
                                            {!! $item->description !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                    <?php endif;
                    endforeach;
                endif; ?>
            </div>
        </div>
    </section>

    <!-- Latest Works -->
    <section class="bg-0 p-t-92 p-b-60">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    LATEST WORKS
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!--  -->
            <div class="row justify-content-center">
                <?php
                $itemOptions = $options->where('name', 'portfolio')->first();
                if(!empty($itemOptions->options)):
                    $optionsArray = json_decode($itemOptions->options);
                    foreach($optionsArray as $option):
                        $item = $items->where('id', $option)->first();
                        if(!empty($item)): ?>
                            <div class="col-sm-10 col-md-8 col-lg-4 p-b-40">
                                <!-- Block2 -->
                                <div class="block2 bg-img2" style="background-image: url({{ !empty($item->image) ? asset('images/upload/portfolio/' . $item->image) : asset('images/project-01.jpg') }});">
                                    <div class="block2-content trans-04">
                                        <h4 class="block2-title t1-m-1 cl-0 flex-s-c trans-04">
                                            {{ $item->title }}
                                        </h4>

                                        <p class="t1-s-2 cl-13 p-b-26">
                                            {!! str_limit($item->description, 100, $end='...') !!}
                                        </p>

                                        <a href="{{ url('insights/portfolio/' . $item->slug) }}" class="d-inline-flex flex-c-c size-a-1 p-rl-15 t1-s-2 text-uppercase cl-6 bg-0 hov-btn3 trans-02">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                    <?php endif;
                    endforeach;
                endif; ?>
            </div>
        </div>
    </section>

    <!-- Blog -->
    <section class="bg-12 p-t-92 p-b-60">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    LATEST BLOG
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!--  -->
            <div class="row justify-content-center">
                <?php
                $itemOptions = $options->where('name', 'blog')->first();
                if(!empty($itemOptions->options)):
                    $optionsArray = json_decode($itemOptions->options);
                    foreach($optionsArray as $option):
                        $item = $items->where('id', $option)->first();
                        if(!empty($item)): ?>
                            <div class="col-sm-10 col-md-8 col-lg-4 p-b-40">
                                <div class="bg-0 h-full">
                                    <a href="{{ url('insights/blog/' . $item->slug) }}" class="hov-img0 of-hidden">
                                        <img src="{{ !empty($item->image) ? asset('images/upload/blog/' . $item->image) : asset('images/news-01.jpg') }}" alt="{{ $item->title }}">
                                    </a>

                                    <div class="bg-0 p-rl-28 p-t-26 p-b-35">
                                        <h4 class="p-b-12">
                                            <a href="{{ url('insights/blog/' . $item->slug) }}" class="t1-m-1 cl-3 hov-link2 trans-02">
                                                {{ $item->title }}
                                            </a>
                                        </h4>

                                        <div class="flex-wr-s-c p-b-9">
                                            <div class="p-r-20">
                                                <i class="fs-14 cl-7 fa fa-calendar m-r-2"></i>

                                                <span class="t1-s-2 cl-7">
                                                    {{ \Carbon\Carbon::parse($item->created_at)->format('M d, Y') }}
                                                </span>
                                            </div>
                                        </div>

                                        <p class="t1-s-2 cl-6 p-b-20">
                                            {!! str_limit($item->description, 100, $end='...') !!}
                                        </p>

                                        <a href="{{ url('insights/blog/' . $item->slug) }}" class="d-inline-flex flex-c-c size-a-1 p-rl-15 t1-s-2 text-uppercase cl-0 bg-11 hov-btn1 trans-02">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                    <?php endif;
                    endforeach;
                endif; ?>
            </div>
        </div>
    </section>

    <!-- Call back -->
    <section class="bg-10 p-t-50 p-b-45">
        <div class="container">
            <form id="conversation-form" class="validate-form p-b-30 p-t-8" method="post" action="{{ url('send-conversation') }}">
                @csrf
                <h3 class="t1-b-1 cl-0 m-b-11 text-center m-b-50">
                    READY TO START A CONVERSATION
                </h3>
                <div class="row">
                    <div class="col-sm-6 p-b-25 validate-input" data-validate="Name is required">
                        <div class="size-a-3">
                            <input class="s-full bg-0 t1-m-2 cl-6 plh-6 p-rl-20" type="text" name="name" placeholder="Your Name *" required>
                        </div>
                    </div>
                    <div class="col-sm-6 p-b-25 validate-input" data-validate="Company name is required">
                        <div class="size-a-3">
                            <input class="s-full bg-0 t1-m-2 cl-6 plh-6 p-rl-20" type="text" name="company_name" placeholder="Company Name *">
                        </div>
                    </div>
                    <div class="col-sm-6 p-b-25 validate-input" data-validate="Email is required">
                        <div class="size-a-3">
                            <input class="s-full bg-0 t1-m-2 cl-6 plh-6 p-rl-20" type="email" name="email" placeholder="Email *" required>
                        </div>
                    </div>

                    <div class="col-sm-6 p-b-25 validate-input" data-validate="Phone is required">
                        <div class="size-a-3">
                            <input class="s-full bg-0 t1-m-2 cl-6 plh-6 p-rl-20" type="text" name="phone" placeholder="Phone *" required>
                        </div>
                    </div>
                    <div class="col-sm-12 p-b-25 validate-input" data-validate="Message is required">
                        <textarea class="size-a-14 t1-m-2 plh-6 cl-6 p-rl-20 p-tb-13 bo-1-rad-4 bcl-12 focus-in1" name="msg" placeholder="Message *" required></textarea>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12 p-b-25">
                        <button class="flex-c-c size-a-4 bg-11 t1-s-2 text-uppercase cl-0 hov-btn2 trans-02 p-rl-15 float-right">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>


    <!-- clients -->
    <section class="bg-0 p-t-92 p-b-40">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-3 txt-center m-b-11">
                    OUR CLIENTS
                </h3>

                <div class="size-a-2 bg-3"></div>
            </div>

            <!-- Client -->
            <div class="row justify-content-center">
                <?php
                $itemOptions = $options->where('name', 'client')->first();
                if(!empty($itemOptions->options)):
                    $optionsArray = json_decode($itemOptions->options);
                    foreach($optionsArray as $option):
                        $item = $items->where('id', $option)->first();
                        if(!empty($item)): ?>
                            <div class="col-sm-4 col-lg-2 flex-c-c p-b-60">
                                <a href="#">
                                    <img class="hov-img2 trans-02 max-s-full" src="{{ !empty($item) ? asset('images/upload/client/' . $item->image) : asset('images/product-01.jpg') }}" alt="{{ $item->title }}">
                                </a>
                            </div>
                    <?php endif;
                    endforeach;
                endif; ?>
            </div>
        </div>
    </section>

    <!-- Testimonials -->
    <section class="parallax100 kit-overlay2 p-t-92 p-b-90" style="background-image: url({{ asset('images/bg-02.jpg') }});">
        <div class="container">
            <!-- Title section -->
            <div class="flex-col-c-c p-b-50">
                <h3 class="t1-b-1 cl-0 txt-center m-b-11">
                    WHAT CLIENT SAY ABOUT US
                </h3>

                <div class="size-a-2 bg-0"></div>
            </div>

            <!-- Slick1 -->
            <div class="wrap-slick1">
                <div class="slide-slick">
                    <?php
                    $itemOptions = $options->where('name', 'testimonial')->first();
                    if(!empty($itemOptions->options)):
                        $optionsArray = json_decode($itemOptions->options);
                        foreach($optionsArray as $option):
                            $item = $items->where('id', $option)->first();
                            if(!empty($item)): ?>
                                <div class="item-slick p-rl-15 wrap-block3">
                                    <div class="block3 d-flex">
                                        <div class="block3-content d-flex">
                                            <div class="block3-pic wrap-pic-w">
                                                <img src="{{ !empty($item->image) ? asset('images/upload/testimonial/' . $item->image) : asset('images/ava-01.jpg') }}" alt="{{ $item->name }}">
                                            </div>

                                            <div class="block3-text d-flex w-full-sr575">
                                                <span class="block3-text-child t1-m-1 text-uppercase cl-0 p-b-4">
                                                    {{ $item->title }}
                                                </span>

                                                <span class="block3-text-child t1-s-3 cl-14 p-b-9">
                                                {{ @json_decode(@$item->options)->designation }}
                                                </span>

                                                <p class="block3-text-child t1-s-2 cl-13">
                                                    {!! $item->description !!}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php endif;
                        endforeach;
                    endif; ?>
                </div>

                <div class="wrap-dot-slick p-t-70"></div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="{{ asset('vendor/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('vendor/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('js/revo-custom.js') }}"></script>
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/conversation.js') }}"></script>
@endsection
