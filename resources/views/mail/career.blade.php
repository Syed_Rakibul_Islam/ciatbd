@component('mail::message')
    @component('mail::panel')
        Career
    @endcomponent
    Name: {{ $name }}
    Work type: {{ ucfirst(str_replace("_", " ", $work_type)) }}
    Work Field: {{ $work_field == 'research' ? 'Research/Consultancy' : 'Marketing' }}
    Work Prefer: {{ $work_prefer == 'home' ? 'Working from home' : 'In office work' }}
    Message: {!! nl2br($description) !!}

    File URL: {{ asset('files/upload/career/'. $file) }}



@endcomponent
