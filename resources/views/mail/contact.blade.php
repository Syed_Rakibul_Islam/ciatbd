@component('mail::message')
    @component('mail::panel')
        Contact Us
    @endcomponent
    User Name: {{ $name }}
    User Email: {{ $email }}
    User Phone: {{ $phone }}
    Message: {!! nl2br($description) !!}
@endcomponent


