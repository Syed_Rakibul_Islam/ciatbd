@component('mail::message')
    @component('mail::panel')
        Conversation
    @endcomponent
    User Name: {{ $name }}
    Company Name: {{ $company_name }}
    User Email: {{ $email }}
    User Phone: {{ $phone }}
    Message: {!! nl2br($description) !!}
@endcomponent


