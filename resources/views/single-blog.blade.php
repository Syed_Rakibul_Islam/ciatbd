@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-07.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Industry Insight
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>
                <a href="{{ url('insights') }}" class="breadcrumb-item">
                    Insights
                </a>
                <a href="{{ url('insights/blog') }}" class="breadcrumb-item">
                    blog
                </a>

                <span class="breadcrumb-item">
					{{ $item->title }}
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <div class="col-sm-10 col-md-8 col-lg-9 p-b-30 m-t-30">
        <div class="p-l-50 p-l-15-sr991 p-l-0-sr767">
            <!-- News detail -->
            <div class="m-b-25">
                @if(!empty($item->image))
                    <a href="{{ url('insights/blog/' . $item->slug) }}" class="m-b-45 max-s-full">
                        <img src="{{ asset('images/upload/blog/' . $item->image) }}" alt="{{ $item->title }}">
                    </a>
                @endif

                <h4 class="t1-b-3 cl-3 m-b-11">
                    {{ $item->title }}
                </h4>

                <div class="flex-wr-s-c m-b-11">
                    <div class="p-r-20">
                        <i class="fs-14 cl-7 fa fa-calendar m-r-2"></i>

                        <span class="t1-s-2 cl-7">
                            {{ \Carbon\Carbon::parse($item->created_at)->format('M d, Y') }}
                        </span>
                    </div>
                </div>

                <div class="t1-s-2 cl-6 m-b-9">
                    {!! $item->description !!}
                </div>
                </div>
            </div>
        </div>
    </div>

@endsection
