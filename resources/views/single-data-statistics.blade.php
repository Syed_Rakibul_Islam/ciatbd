@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-07.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Data & Statistics
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>
                <a href="{{ url('insights') }}" class="breadcrumb-item">
                    Insights
                </a>
                <a href="{{ url('insights/data-statistics') }}" class="breadcrumb-item">
                    Data & Statistics
                </a>

                <span class="breadcrumb-item">
					{{ $item->title }}
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <section class="bg-0 p-t-30 p-b-40">
        <div class="container" >
            <div class="row justify-content-center">
                <div class="col-lg-12 p-b-50">
                    <div class="bg-0 h-full">

                        <div class="bg-0 p-t-26 p-b-35">
                            <h4 class="p-b-12 text-center">
                                <a href="{{ url('insights/data-statistics/' . $item->slug) }}" class="t1-m-1 cl-3 hov-link2 trans-02 text-uppercase">
                                    {{ $item->title }}
                                </a>
                            </h4>

                            <p class="t1-s-2 cl-6 p-b-20">
                                {!! $item->description !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-0 p-t-20 p-b-40">
        <div class="container" >
            <div class="row justify-content-center">
                @foreach($categories as $category)
                    <div class="col-sm-6 col-lg-4 p-b-45" >
                        <div class="flex-col-s-c">
                            <div class="w-full pos-relative wrap-pic-w m-b-16">
                                <img src="{{ asset('images/upload/data-statistics/category/' . $category->image) }}" alt="{{ $category->title }}">

                                <div class="s-full ab-t-l flex-col-c-c hov-1 p-tb-30 p-rl-15">
                                    <a href="{{ @json_decode(@$category->options)->url }}" class="size-a-15 d-inline-flex flex-c-c bg-11 t1-s-2 text-uppercase cl-0 hov-btn2 trans-02 p-rl-10 hov-1-2" download>
                                        Download
                                    </a>
                                </div>
                            </div>

                            <h3 class="t1-m-1 cl-3 trans-02">
                                {{ $category->title }}
                            </h3>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </section>

@endsection
