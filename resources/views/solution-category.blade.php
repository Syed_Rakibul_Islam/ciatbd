@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-06.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Solutions
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>
                <span class="breadcrumb-item">
					<a href="{{ url('/solutions') }}" class="breadcrumb-item">
                        Solutions
                    </a>
				</span>

                <span class="breadcrumb-item">
					{{ $item->slug }}
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <section class="bg-0 p-t-30 p-b-40 {{ @$solution_type == 'category_black' ? 'bg-blue' : '' }}">
        <div class="container" >
            <div class="row justify-content-center">
                <div class="col-sm-10 p-b-60">
                    <h1 class="t1-b-1 text-center m-b-25 text-uppercase" >{{ $item->title }}</h1>
                    <div class="text-center">{!! $item->description !!}</div>
                </div>

                @foreach($item->categories as $category)
                    <div class="col-sm-10 col-md-6 p-t-20">
                        <div class="text-center">
                            @if(!empty($category->image))
                                <img src="{{ asset('images/upload/solution/category/' . $category->image) }}" width="96">
                            @endif
                            <div class="justify-content-center p-t-20">{!! $category->description !!}</div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

@endsection
