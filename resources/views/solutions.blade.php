@extends('layouts.app')

@section('content')
    <!-- Title page -->
    <section class="bg-img1 kit-overlay1" style="background-image: url({{ url('images/bg-06.jpg') }});">
        <div class="container size-h-3 p-tb-30 flex-col-c-c">
            <h2 class="t1-b-1 text-uppercase cl-0 txt-center m-b-25">
                Solutions
            </h2>

            <div class="flex-wr-c-c">
                <a href="{{ url('/') }}" class="breadcrumb-item">
                    Home
                </a>

                <span class="breadcrumb-item">
					Solutions
				</span>
            </div>
        </div>

    </section>

    <!-- Contact -->

    <section class="bg-0 p-t-30 p-b-40">
        <div class="container" >
            <div class="row justify-content-center">
            @foreach($items as $item)
                <div class="col-sm-10 col-md-6 p-b-60">
                    <h1 class="t1-b-1 text-center m-b-25 text-uppercase" ><a href="{{ url('solutions/' . $item->slug) }}">{{ $item->title }}</a></h1>
                    <div class="text-center">{!! str_limit($item->description, 150, $end='...<a href="' . url('solutions/' . $item->slug) . '">Read more</a>') !!}</div>
                </div>

            @endforeach
            </div>
        </div>
    </section>

@endsection
