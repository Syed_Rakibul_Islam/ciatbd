<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Admin Panel
Route::prefix('admin')->group(function () {
    // Authentication Routes...
    $this->get('login', 'Admin\Auth\AdminLoginController@showAdminLoginForm')->name('admin.login');
    $this->post('login', 'Admin\Auth\AdminLoginController@login');
    $this->post('logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');

    // Password Reset Routes...
    $this->get('password/reset', 'Admin\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    $this->post('password/email', 'Admin\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    $this->get('password/reset/{token}', 'Admin\Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    $this->post('password/reset', 'Admin\Auth\AdminResetPasswordController@reset');

    Route::group(['middleware' => ['isAdmin']], function () {

        Route::get('dashboard', 'Admin\DashboardController@index');
        Route::get('/', 'Admin\DashboardController@index');

        /*Profile module*/
        Route::get('/profile', 'Admin\AdminProfileController@index');
        Route::get('/profile/edit', 'Admin\AdminProfileController@profileEdit');
        Route::post('/profile/edit', 'Admin\AdminProfileController@profileSave');
        Route::get('/profile/change-password', 'Admin\AdminProfileController@changePassword');
        Route::post('/profile/change-password', 'Admin\AdminProfileController@savePassword');

        Route::get('slider/sorting', 'Admin\SliderController@sorting');
        Route::post('slider/sorting', 'Admin\SliderController@saveSorting');
        Route::resource('slider', 'Admin\SliderController');
        Route::post('slider/{id}/publish', 'Admin\SliderController@publish');
        Route::post('slider/{id}/unpublish', 'Admin\SliderController@unpublish');

        Route::get('about/sorting', 'Admin\AboutController@sorting');
        Route::post('about/sorting', 'Admin\AboutController@saveSorting');
        Route::resource('about', 'Admin\AboutController');
        Route::post('about/{id}/publish', 'Admin\AboutController@publish');
        Route::post('about/{id}/unpublish', 'Admin\AboutController@unpublish');

        Route::get('why-choose/sorting', 'Admin\WhyChooseController@sorting');
        Route::post('why-choose/sorting', 'Admin\WhyChooseController@saveSorting');
        Route::resource('why-choose', 'Admin\WhyChooseController');
        Route::post('why-choose/{id}/publish', 'Admin\WhyChooseController@publish');
        Route::post('why-choose/{id}/unpublish', 'Admin\WhyChooseController@unpublish');

        Route::get('team/sorting', 'Admin\TeamController@sorting');
        Route::post('team/sorting', 'Admin\TeamController@saveSorting');
        Route::resource('team', 'Admin\TeamController');
        Route::post('team/{id}/publish', 'Admin\TeamController@publish');
        Route::post('team/{id}/unpublish', 'Admin\TeamController@unpublish');

        Route::get('solution/sorting', 'Admin\SolutionController@sorting');
        Route::post('solution/sorting', 'Admin\SolutionController@saveSorting');
        Route::resource('solution', 'Admin\SolutionController');
        Route::post('solution/{id}/publish', 'Admin\SolutionController@publish');
        Route::post('solution/{id}/unpublish', 'Admin\SolutionController@unpublish');
        Route::resource('solution/{post_id}/category', 'Admin\SolutionCategoryController');
        Route::post('solution/{post_id}/category/{id}/publish', 'Admin\SolutionCategoryController@publish');
        Route::post('solution/{post_id}/category/{id}/unpublish', 'Admin\SolutionCategoryController@unpublish');

        Route::resource('insight', 'Admin\InsightController')->except(['create', 'store', 'destroy']);
        Route::post('insight/{id}/publish', 'Admin\InsightController@publish');
        Route::post('insight/{id}/unpublish', 'Admin\InsightController@unpublish');

        Route::get('client/sorting', 'Admin\ClientController@sorting');
        Route::post('client/sorting', 'Admin\ClientController@saveSorting');
        Route::resource('client', 'Admin\ClientController');
        Route::post('client/{id}/publish', 'Admin\ClientController@publish');
        Route::post('client/{id}/unpublish', 'Admin\ClientController@unpublish');

        Route::get('testimonial/sorting', 'Admin\TestimonialController@sorting');
        Route::post('testimonial/sorting', 'Admin\TestimonialController@saveSorting');
        Route::resource('testimonial', 'Admin\TestimonialController');
        Route::post('testimonial/{id}/publish', 'Admin\TestimonialController@publish');
        Route::post('testimonial/{id}/unpublish', 'Admin\TestimonialController@unpublish');

        Route::get('data-statistics/sorting', 'Admin\DataStatisticsController@sorting');
        Route::post('data-statistics/sorting', 'Admin\DataStatisticsController@saveSorting');
        Route::resource('data-statistics', 'Admin\DataStatisticsController');
        Route::post('data-statistics/{id}/publish', 'Admin\DataStatisticsController@publish');
        Route::post('data-statistics/{id}/unpublish', 'Admin\DataStatisticsController@unpublish');
        Route::resource('data-statistics/{post_id}/category', 'Admin\DataStatisticsCategoryController');
        Route::post('data-statistics/{post_id}/category/{id}/publish', 'Admin\DataStatisticsCategoryController@publish');
        Route::post('data-statistics/{post_id}/category/{id}/unpublish', 'Admin\DataStatisticsCategoryController@unpublish');

        Route::get('industry-insight/sorting', 'Admin\IndustryInsightController@sorting');
        Route::post('industry-insight/sorting', 'Admin\IndustryInsightController@saveSorting');
        Route::resource('industry-insight', 'Admin\IndustryInsightController');
        Route::post('industry-insight/{id}/publish', 'Admin\IndustryInsightController@publish');
        Route::post('industry-insight/{id}/unpublish', 'Admin\IndustryInsightController@unpublish');
        Route::resource('industry-insight/{post_id}/category', 'Admin\IndustryInsightCategoryController');
        Route::post('industry-insight/{post_id}/category/{id}/publish', 'Admin\IndustryInsightCategoryController@publish');
        Route::post('industry-insight/{post_id}/category/{id}/unpublish', 'Admin\IndustryInsightCategoryController@unpublish');

        Route::get('portfolio/sorting', 'Admin\PortfolioController@sorting');
        Route::post('portfolio/sorting', 'Admin\PortfolioController@saveSorting');
        Route::resource('portfolio', 'Admin\PortfolioController');
        Route::post('portfolio/{id}/publish', 'Admin\PortfolioController@publish');
        Route::post('portfolio/{id}/unpublish', 'Admin\PortfolioController@unpublish');
        Route::resource('portfolio/{post_id}/category', 'Admin\PortfolioCategoryController');
        Route::post('portfolio/{post_id}/category/{id}/publish', 'Admin\PortfolioCategoryController@publish');
        Route::post('portfolio/{post_id}/category/{id}/unpublish', 'Admin\PortfolioCategoryController@unpublish');

        Route::get('blog/sorting', 'Admin\BlogController@sorting');
        Route::post('blog/sorting', 'Admin\BlogController@saveSorting');
        Route::resource('blog', 'Admin\BlogController');
        Route::post('blog/{id}/publish', 'Admin\BlogController@publish');
        Route::post('blog/{id}/unpublish', 'Admin\BlogController@unpublish');

        Route::get('workshop-and-interview/sorting', 'Admin\WorkshopAndInterviewController@sorting');
        Route::post('workshop-and-interview/sorting', 'Admin\WorkshopAndInterviewController@saveSorting');
        Route::resource('workshop-and-interview', 'Admin\WorkshopAndInterviewController');
        Route::post('workshop-and-interview/{id}/publish', 'Admin\WorkshopAndInterviewController@publish');
        Route::post('workshop-and-interview/{id}/unpublish', 'Admin\WorkshopAndInterviewController@unpublish');
        Route::resource('workshop-and-interview/{post_id}/category', 'Admin\WorkshopAndInterviewCategoryController');
        Route::post('workshop-and-interview/{post_id}/category/{id}/publish', 'Admin\WorkshopAndInterviewCategoryController@publish');
        Route::post('workshop-and-interview/{post_id}/category/{id}/unpublish', 'Admin\WorkshopAndInterviewCategoryController@unpublish');

        Route::get('contact', 'Admin\ContactController@index');
        Route::get('contact/edit', 'Admin\ContactController@edit');
        Route::post('contact', 'Admin\ContactController@update');

        Route::get('dashboard', 'Admin\DashboardController@index');

    });

});


//Auth::routes();

Route::group(['middleware' => ['share']], function () {
    Route::get('{home}', 'HomeController@index')->where('home', '|home|dashboard');
    Route::get('about', 'HomeController@about');
    Route::get('solutions', 'HomeController@solutions');
    Route::get('insights', 'HomeController@insights');
    Route::get('insights/data-statistics', 'HomeController@dataStatistics');
    Route::get('insights/data-statistics/{slug}', 'HomeController@singleDataStatistics');
    Route::get('insights/industry-insight', 'HomeController@industryInsight');
    Route::get('insights/industry-insight/{slug}', 'HomeController@singleIndustryInsight');
    Route::get('insights/portfolio', 'HomeController@portfolio');
    Route::get('insights/portfolio/{slug}', 'HomeController@singlePortfolio');
    Route::get('insights/blog', 'HomeController@blog');
    Route::get('insights/blog/{slug}', 'HomeController@singleBlog');
    Route::get('insights/workshop-and-interview', 'HomeController@workshopAndInterview');
    Route::get('insights/workshop-and-interview/{slug}', 'HomeController@singleWorkshopAndInterview');
    Route::get('clients', 'HomeController@clients');
    Route::get('solutions/{slug}', 'HomeController@solution');
    Route::view('career', 'career');
    Route::post('career', 'HomeController@sendCareer');
    Route::get('contact', 'HomeController@contact');
});

Route::post('send-contact', 'SendMailController@sendContact');
Route::post('send-conversation', 'SendMailController@sendConversation');
